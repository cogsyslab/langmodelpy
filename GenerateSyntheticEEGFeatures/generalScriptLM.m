%calibrationFileERPxx.mat file that contains scalar EEG scores for +/- calibration trials from three users with AUC levels around
%70 Low
%78 Medium
%85 High

aucLevel='low'; %low,medium,high
numSamplesTarget=1; % number of Samples you want to drwan
numSamplesNonTarget=27;

if(strcmp(aucLevel,'low'))
    load('calibrationFileERPLow.mat');
elseif(strcmp(aucLevel,'medium'))
    load('calibrationFileERPMedium.mat');
else
    load('calibrationFileERPHigh.mat')
end

targetScores=scoreStruct.conditionalpdf4targetKDE.data;
nontargetScores= scoreStruct.conditionalpdf4nontargetKDE.data;

% An evalKDE.m script that evaluates the likelihood of an input scores
conditionalpdf4targetKDE=kde1d(targetScores);%%
conditionalpdf4nontargetKDE=kde1d(nontargetScores); %%

%A drawKDE.m script that draws a specified number of samples from +/- KDEs with the specified parameters/data; e.g. given N+ and N- and a specified data file, draw samples.
fileID = fopen(strcat('EEGEvidence.txt-', aucLevel), 'w');
for i=1:1000
    targetSamples=drawKDE(numSamplesTarget,conditionalpdf4targetKDE);
    nonTargetSamples=drawKDE(numSamplesNonTarget,conditionalpdf4nontargetKDE);

    targetEEGEvidence=log(conditionalpdf4targetKDE.probs(targetSamples))- log(conditionalpdf4nontargetKDE.probs(targetSamples));
    nontargetEEGEvidence=log(conditionalpdf4targetKDE.probs(nonTargetSamples))- log(conditionalpdf4nontargetKDE.probs(nonTargetSamples));
    fprintf(fileID, '%f\n', targetEEGEvidence);
    fprintf(fileID, '%f\n', nontargetEEGEvidence);
    fprintf(fileID, '\n');
end
fclose(fileID);