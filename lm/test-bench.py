#!/usr/bin/python

import numpy as np
from scipy.stats import norm
import argparse, math
import lm_server
import sys
import pywrapfst as fst
import Levenshtein

class EEGSyntheticData:
    def __init__(self, filename):
        self.data = []
        sample = []
        with open(filename, 'r') as f:
            for line in f:
                line = line.strip()
                if line:
                    sample.append(eval(line))
                else:
                    # Normalizes the sample and convert it to negative log space.
                    probs = [math.e**prob for prob in sample]
                    denom = sum(probs)
                    probs = map(lambda x: -math.log(x/denom), probs)
                    self.data.append(probs)
                    sample = []
        self.idx = np.random.permutation(len(self.data))
        self.count = 0  # Indicates the next available sample.

    def get_next_sample(self):
        sample = self.data[self.idx[self.count]]
        self.count += 1
        if self.count >= len(self.data):
            self.count = 0
        return sample

    def reset(self):
        self.count = 0


#
# Draw the distribution.
#
# INPUTS:
#   dist, a sorted array of 2-tuple in the form of (char, probability).
#
#   rescale, a boolean which indicates whether we should rescale the probability
#   from minus logratihm space. The default value is True.
#
# OUTPUTS:
#   Print the ditribution on the screen.
#
def draw_distribution(dist, rescale=True):
    if rescale:
        dist = [[char, math.e**(-prob)] for char, prob in dist]
    print "===================================================================="
    for char, prob in dist:
        #print "{0} ({1:.3f}) {2}'".format(char, prob, 'x' * int(prob * 100))
        print "{0} ({1:.3f})'".format(char, prob)
    print "===================================================================="

def draw_dist(dist, sym, rescale=True):
    if rescale:
        dist = [[char, math.e**(-prob)] for char, prob in dist]
    print "===================================================================="
    for k, (char, prob) in enumerate(dist):
        if char==ch:
            print "\033[94m\033[1m{0} ({1:.3f})'\033[0;0m".format(char, prob)
            rank = k+1
        else:
        #print "{0} ({1:.3f}) {2}'".format(char, prob, 'x' * int(prob * 100))
            print "{0} ({1:.3f})'".format(char, prob)
    print "===================================================================="
    return rank

# Parses the commandline argument.
parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group.add_argument('--copy', action='store_true',
        help='Simulate the copy task')
group.add_argument('--gen', action='store_true',
        help='Generate random word from language model')
group.add_argument('--random', action='store_true',
        help='Add random noise to the choice')
parser.add_argument('--reseed', action='store_true',
        help='reseed the random generator')
parser.add_argument('--sim', dest='sim', action='store_true',
                    help='simulates word generation task with eeg')
parser.add_argument('--lm', dest='lm', type=str,
        help='path to the language model')
parser.add_argument('--eeg', help='path to EEG synthetic data.')

args = parser.parse_args()
if not args.lm:
    print "the language model must be specified. see --h for help."

# Reseeds the random generator.
if args.reseed:
    np.random.seed()
else:
    np.random.seed(1437)

# Initialize the language model object
lm = lm_server.server(args.lm)

# Initializes the available charset
charset = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
           'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '<', '#']

# Simulates the copy task. The prompt will ask user to type a word for copying.
# Then the program will show the distribution given by language model for each
# character. But the choice will always be exactly the same as the character at
# corresponding position of the original word.
if args.copy:
    word = raw_input("Enter a word for the copy task: ")
    user_typed = ""
    ranks = []
    for ch in word:
        if ch == " ":
            ch="#"
        # Retrieves the prior distribution over the next character and show it.
        next_ch_dist = lm.get_prior()
        rank = draw_dist(next_ch_dist, ch, False)
        ranks.append(rank)
        # Updates the language model with our choice.
        lm.update_symbol(ch)

        if ch=="#":
            ch=" "
        user_typed += ch

        print user_typed + "_" * (len(word) - len(user_typed))
    print "average rank is: ",np.mean(ranks)
    # var = mean(abs(x - x.mean())**2)
    print "variance in ranks is: ",np.var(ranks)


# Simulates the generation task. The prompt will ask user to enter the expected
# length of a word. The program will random a choice from a-z and then keep
# picking the character of the highest probability
if args.gen:
    length = eval(raw_input("Enter the expected length of a word: "))
    user_typed = ""
    random_index = np.random.randint(0, 26)

    # Randoms the first character and update the language model with it.
    ch = chr(ord('a') + random_index)
    user_typed += ch
    lm.update_symbol(ch)
    print user_typed + "_" * (length - len(user_typed))

    # Complete the rest of word.
    for i in range(length - 1):
        # Retrieves the prior distribution over the next character and show it.
        next_ch_dist = lm.get_prior()
        draw_distribution(next_ch_dist, False)

        # Updates the language model with the character of the highest
        # probability.
        ch = next_ch_dist[0][0]
        user_typed += ch
        print user_typed + "_" * (length - len(user_typed))
        lm.update_symbol(ch)

# Similar to the copy task. However, we also add a Gaussian noise to the
# supposed character. Then we use MAP to get the next one.
if args.random:
    word = raw_input("Enter a word for the copy task: ")
    user_typed = ""
    for ch in word:
        # Retrieves the prior distribution over the next character and show it.
        next_ch_dist = lm.get_prior()
        draw_distribution(next_ch_dist, False)

        # Generates the pdf of a Gaussian distribution which centers at the
        # supposed character and has a standard deviation of 3.
        center = ord(ch) - ord('a')
        choice_dist = map(lambda x: norm.pdf(x, center, 2), range(0, 26))

        # Rescales the pdf to minus logarithm and use a dictionary to facilitate
        # inquiry.
        posterior_dist = \
            {chr(ord('a') + i):-math.log(choice_dist[i]) for i in range(26)}

        # Uses Bayesian theorem to compute the posterior probability
        for c, prob in next_ch_dist:
            if not c in posterior_dist:
                continue
            posterior_dist[c] += prob
        print posterior_dist

        # Find the minimum (equivalent to MAP because we are using minus log of
        # probability.
        choice = min(posterior_dist, key=posterior_dist.get)
        user_typed += choice
        print user_typed + "-" * (len(word) - len(user_typed))
        lm.update_symbol(choice)

if args.sim:
    #load confusion data
    eeg = EEGSyntheticData(args.eeg)
    #insert simulation sentence
    sentence = raw_input("Enter a sentence for the generation task: ")
    # We iterate over the symbols in the sentence string.
    # Stores the error number of the whole sentence.
    sentence_err = 0
    ranks = {}
    # Always Pick the best from EEG.
    eeg_best = ""
    ranks['eeg_best'] = []
    for i in xrange(len(sentence)):
        ch = sentence[i]
        if ch == " ": ch = "#"
        sample = eeg.get_next_sample()
        rank = draw_dist(sorted(zip(charset, sample), key=lambda x:x[1]), ch, False)
        ranks['eeg_best'].append(rank)
        # The EEG decision
        ch = charset[sample.index(min(sample))]
        if ch == "#": ch = " "
        eeg_best += ch
        print eeg_best + "_" * (len(sentence) - len(eeg_best))
    # Create a lattice from EEG samples, rescore it with the language model.
    '''
    eeg_rescored = ""
    ranks['eeg_rescored'] = []
    eeg.reset()
    eeg_synthetic_fst = fst.Fst()
    eeg_synthetic_fst.set_input_symbols(lm.lm_syms)
    eeg_synthetic_fst.set_output_symbols(lm.lm_syms)
    eeg_synthetic_fst.add_state()
    eeg_synthetic_fst.set_start(0)
    for i in xrange(len(sentence)):
        sample = eeg.get_next_sample()
        for j in xrange(len(sample)):
            if charset[j] == "<": continue  # Doesn't consider backspace right now.
            key = lm.lm_syms.find(charset[j])
            eeg_synthetic_fst.add_arc(i, fst.Arc(key, key, sample[j], i+1))
        eeg_synthetic_fst.add_state()
    eeg_synthetic_fst.set_final(len(sentence))
    eeg_synthetic_fst.arcsort(st="olabel")
    print "Intersecting..."
    output_fst = fst.intersect(eeg_synthetic_fst, lm.lm)
    print "Removing epsilon..."
    output_fst = fst.rmepsilon(output_fst)
    print "Determinizing..."
    output_fst = fst.determinize(output_fst)
    print "Minimizing..."
    output_fst.minimize()
    print "Finding shortest path..."
    path = fst.shortestpath(output_fst)
    for state in path.states():
        for arc in path.arcs(state):
            ch = lm.lm_syms.find(arc.ilabel)
            assert len(ch) == 1
            if ch == "#": ch = " "
            eeg_rescored += ch
    print eeg_rescored
    '''
    # Simluates the actual typing process.
    user_typed = sentence[0]
    lm.update_symbol(sentence[0])
    ranks['simulation'] = []
    eeg.reset()
    # Skips the first symbol 'cause we assume to know it in this test case.
    eeg.get_next_sample()
    for i in xrange(1,len(sentence)):
        # make sure we start from second letter
        ch = sentence[i]
        if ch == ' ': ch = '#'
        # Generates eeg vector
        next_ch_dist_eeg = eeg.get_next_sample()
        # Retrieves the prior distribution over the next character and show it.
        next_ch_dist_lm = lm.get_prior_array()
        # Sums up lm and eeg (on log scale)
        eeg_lm_dist = sorted(zip(charset, map(lambda (x, y): x + y, zip(next_ch_dist_eeg, next_ch_dist_lm))),
                             key=lambda prob: prob[1])
        # Extracts the highest (lowest on log scale)
        symbol = eeg_lm_dist[0][0]
        print "ALL"
        rank = draw_dist(eeg_lm_dist, ch, False)
        ranks['simulation'].append(rank)
        # for visalizing components
        print "EEG"
        draw_dist(sorted(zip(charset, next_ch_dist_eeg), key=lambda x:x[1]), ch, False)
        print "LM"
        draw_dist(sorted(zip(charset, next_ch_dist_lm), key=lambda x:x[1]), ch, False)
        # update from eeg regarding the final decision
        lm.update_symbol(symbol)
        if symbol == '#': symbol = ' '
        user_typed += symbol
        print user_typed + "_" * (len(sentence) - len(user_typed))
        #after loop measure error rate
    eeg_best_edit_dist = Levenshtein.distance(eeg_best, sentence)
    user_typed_edit_dist = Levenshtein.distance(user_typed, sentence)
    print "======================== RESULTS ================================"
    print "Sentence to copy: " + sentence
    print ""
    print "BEST EEG:"
    print "    mean rank: {0}, var: {1}".format(np.mean(ranks["eeg_best"]), np.var(ranks["eeg_best"]))
    print "    edit distance: {0}/{1} ({2})".format(eeg_best_edit_dist, len(sentence), eeg_best_edit_dist / float(len(sentence)))
    print ""
    print "SIMULATION:"
    print "    mean rank: {0}, var: {1}".format(np.mean(ranks["simulation"]), np.var(ranks["simulation"]))
    print "    edit distance: {0}/{1} ({2})".format(user_typed_edit_dist, len(sentence), user_typed_edit_dist / float(len(sentence)))
    print "================================================================="
    '''
    sentence_err = lm_server.error_rate(sentence, user_typed)
    # output the total error rate:
    print "The sentence: '{0}' has : {1:.2f}% error rate".format(
            sentence, float(sentence_err) / len(sentence[1:]) * 100)
    print "the prediction: '{}'".format(user_typed)
    '''

