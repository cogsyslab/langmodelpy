from __future__ import division
import sys
import subprocess
import random
from bitweight import *
import math
import yaml
from collections import defaultdict
import csv
import pywrapfst as fst

class lm_demo:
    def __init__(self, filename):
        # read LM
        self.lm = fst.Fst.read(filename)
        self.lm.arcsort(st="ilabel")
        # read symbols
        self.lm_syms = self.lm.input_symbols()
        # Sigma machine
        self.sigma = None
        # History array of this typing process.
        self.history = []
        # Do the actual initialization.
        self.init()
        # Set up the valid characters and corresponding indices.
        self.legit_ch_dict = {\
                'a':0,\
                'b':1,\
                'c':2,\
                'd':3,\
                'e':4,\
                'f':5,\
                'g':6,\
                'h':7,\
                'i':8,\
                'j':9,\
                'k':10,\
                'l':11,\
                'm':12,\
                'n':13,\
                'o':14,\
                'p':15,\
                'q':16,\
                'r':17,\
                's':18,\
                't':19,\
                'u':20,\
                'v':21,\
                'w':22,\
                'x':23,\
                'y':24,\
                'z':25,\
                '<':26,\
                '#':27} # Space is represented as '#'

    def init(self):
        # build sigma
        self.sigma = fst.Fst()
        self.sigma.set_input_symbols(self.lm_syms)
        self.sigma.set_output_symbols(self.lm_syms)
        self.sigma.add_state()
        self.sigma.set_start(0)
        for code, ch in self.lm_syms:
            if code == 0:  # Don't include epsilon in sigma machine.
                continue
            self.sigma.add_arc(0, fst.Arc(code, code, None, 1))
        self.sigma.add_state()
        self.sigma.set_final(1)
        
        # since there's no history do not concat sigma with anything.
        # The history for now will be an array of characters,
        # because for creating arcs in pyfst, it needs a string instead of the
        # integer id.
        self.history = []

    def undo(self):
        '''
        Undo previous typing. This effectively deletes the last character
        in typing history.
        '''
        self.history = self.history[:-1]

    def get_prior_array(self):
        '''
        Get negative-log space probability distribution over the next character.

        OUTPUTS:
            an array of minus log probabilities, sorted according to the value
            in self.legit_ch_dict.
        '''
        sorted_prior = self.get_prior()
        sorted_by_key = sorted(sorted_prior, \
                key=lambda prior:self.legit_ch_dict[prior[0]])
        probs = [prob for _, prob in sorted_by_key]
        # Manually adds the probability of backspace to the array, which is
        # always 0 for now.
        probs.insert(self.legit_ch_dict['<'], 100.00)
        return probs

    def get_real_prior_array(self):
        '''
        Get real-space probability distribution over the next character.

        OUTPUTS:
            an array of real probabilities, sorted according to the value in
            self.legit_ch_dict.
        '''
        probs = self.get_prior_array()
        return [math.exp(-p) for p in probs]


    def get_prior(self):
        '''
        set an array with priors
        in future priors are given from rsvp EEG vector

        OUTPUTS:
            an array of tuples, which consists of the character and the
            corresponding probabilities.
        '''
        sigma_h = self.create_machine_history()

        # intersect
        sigma_h.arcsort(st="olabel")
        output_dist = fst.intersect(sigma_h, self.lm)

        # process result
        output_dist = fst.rmepsilon(output_dist)
        output_dist = fst.determinize(output_dist)
        output_dist.minimize()
        output_dist = fst.push(output_dist, push_weights=True, to_final=True)

        # worth converting this history to np.array if vector computations
        # will be involeved
        #output_dist.arcsort(sort_type="olabel")

        # traverses the shortest path until we get to the second to
        # last state. And the arcs from that state to the final state contain
        # the distribution that we want.
        prev_stateid = curr_stateid = None
        for state in output_dist.states():
            if not curr_stateid is None:
                prev_stateid = curr_stateid
            curr_stateid = state
        priors = []
        for arc in output_dist.arcs(prev_stateid):
            ch = self.lm_syms.find(arc.ilabel) #ilabel and olabel are the same.
            w = float(arc.weight)

            # TODO: for this demo we only need distribution over the characters
            # from 'a' to 'z'
            if len(ch) == 1 and ch in self.legit_ch_dict:
                priors.append((ch, w))

        # assuming the EEG input is an array like [("a", 0.3),("b", 0.2),...]
        # sort the array depending on the probability
        priors = sorted(priors, key=lambda prior: prior[1])
        normalized_dist = self._normalize([prob for _, prob in priors])
        return zip([ch for ch, _ in priors], normalized_dist)

    def update_symbol(self, decision):
        '''
        update history given new input from rsvp
        add arc to last state
        '''
        self.history.append(decision)

    def get_history(self):
        '''
        Return the symbols in the history.
        '''
        return self.history

    def create_machine_history(self):
        '''
        Creates a fst which contains the history and ready to compute
        the distribution over next character.
        '''
        # initiate sigma_h with sigma
        sigma_h = self.sigma
        if self.history:
            # build fst with previous "chosen" characters
            sigma_h = fst.Fst()
            sigma_h.set_input_symbols(self.lm_syms)
            sigma_h.set_output_symbols(self.lm_syms)
            sigma_h.add_state()
            sigma_h.set_start(0)
            for i, ch in enumerate(self.history):
                key = self.lm_syms.find(ch)
                sigma_h.add_arc(i, fst.Arc(key, key, None, i+1))
                sigma_h.add_state()
            sigma_h.set_final(i+1)

            # add sigma at the end
            sigma_h.concat(self.sigma)
            sigma_h = fst.rmepsilon(sigma_h)
        return sigma_h

    def symbol(self):
        symbols = self.legit_ch_dict.keys()
        for i in xrange(len(symbols)):
            if symbols[i] == '#':
                symbols[i] = ' '
        return symbols


    def _normalize(self, distribution):
        '''
        Normalize the distribution which is in negative log (e based) space.

        INPUTS:
            distribution, an array of floating values which are in negative log
            (e based space).

        OUTPUTS:
            an array of normalized distribution in negative log (e based) space.
        '''
        transformed_dist = [BitWeight(math.e**(-prob)) for prob in distribution]
        total = sum(transformed_dist)
        normalized_dist = [(prob / total).to_real for prob in transformed_dist]
        return [-math.log(prob) for prob in normalized_dist]


class eeg_demo:
    def __init__(self, config):
        with open(config, "r") as f: # load config file
            conf = yaml.load(f)

        # load symbols
        filename = open(conf["symbols"], "r")
        syms = fst.SymbolTable()
        syms.add_symbol("<eps>")
        for sym in filename.readlines():
            syms.add_symbol(sym.strip())
        self.symbols = syms
        # read csv file to a sparse dict
        confs = defaultdict(lambda: defaultdict(float))
        filename = open(conf["csv"],"r")

        reader = csv.reader(filename)
        for row in reader:
            confs[row[0]][row[1]] = -math.log(float(row[2]))
            confs[row[1]][row[0]] = -math.log(float(row[2]))

        self.confs = confs
        self.create_eegfst()

    def create_eegfst(self):
        """build an FST with three main branches. One branch with probability alpha
        that has pr. one for each symbol to be prediced given the symbol.
        A second branch, with pr beta is a distributed version of confused symbols.
        And a third, with pr gamma is a unity distribution over all symbols given
        the current symbol."""

        symbols = self.symbols
        
        confs = self.confs
        # determine alpha beta gamma (could be sampled in future)

        alpha = -math.log(0.35)
        beta = -math.log(0.35)
        gamma = -math.log(0.3)
        #alpha = -math.log(0.0001)
        #beta = -math.log(0.0001)
        #gamma = -math.log(0.9998)


        # build fst log - because determinize adds up pr and not choose min pr
        eegfst = fst.Fst()
        eegfst.add_state()
        eegfst.set_start(0)

        # correct Transducer
        epsid = symbols.find("<eps>")
        eegfst.add_arc(0, fst.Arc(epsid, epsid, alpha, 1))
        eegfst.add_state()
        eegfst.add_arc(1, fst.Arc(epsid, epsid, 0, 2))
        eegfst.add_state()


        for key, sym in symbols:
            if sym=="<eps>":
                continue
            eegfst.add_arc(1, fst.Arc(key, key, -math.log(1), 1))            

        # confused Transducer
        eegfst.add_arc(0, fst.Arc(epsid, epsid, beta, 3))
        eegfst.add_state()
        eegfst.add_arc(3, fst.Arc(epsid, epsid, 0, 2))

        for key, sym in symbols:
            if sym=="<eps>":
                continue
            try:
                confs[sym]
                for value in confs[sym]: # use confusions
                    pr = confs[sym][value]
                    vald = symbols.find(value)
                    eegfst.add_arc(3, fst.Arc(key, vald, pr, 3))
            except:
                eegfst.add_arc(3, fst.Arc(key, key, -math.log(1), 3)) # keep original pr
                
        # random Transducer
        eegfst.add_arc(0, fst.Arc(epsid, epsid, gamma, 4))
        eegfst.add_state()
        eegfst.add_arc(4, fst.Arc(epsid, epsid, 0, 2))
        
        for key1, sym1 in symbols:
            if sym1=="<eps>":
                continue
            for key2, sym2 in symbols:
                if sym2=="<eps>":
                    continue
                uniform = symbols.num_symbols()-1 # no epsilon
                eegfst.add_arc(4, fst.Arc(key1, key2, -math.log(1/uniform), 4))
                
                
        eegfst.set_final(2)
        self.eegfst = eegfst


    def get_observation(self, ch):
        '''
        Produce a simulated EEG signal. The output vector
        will be the intersection of the current ch with eeg vector.
        Then output is normalized.
        INPUT:
            a letter symbol
        OUTPUT:
            a noisified array simulating eeg
        '''
        syms = self.symbols
        eegfst = self.eegfst
        eegfst.set_input_symbols(syms)
        eegfst.set_output_symbols(syms)

        # construct char fst
        char = fst.Fst()
        char.set_input_symbols(syms)
        char.set_output_symbols(syms)
        key = syms.find(ch)
        char.add_state()
        char.set_start(0)
        char.add_arc(0, fst.Arc(key, key, 0, 1))
        char.add_state()
        char.set_final(1)

        #compose both
        out = fst.compose(char, eegfst)
        out = fst.rmepsilon(out)
        outmap = fst.statemap(out)
        encoder = fst.EncodeMapper(outmap.arc_type(), encode_labels=True)
        outmap.encode(encoder)
        outmap_det = fst.determinize(outmap)
        outmap_det.minimize()
        outmap_det.decode(encoder)

        # they are not log scale so sum is straight forward
        arc_dict = {}

        for state in outmap_det.states():
            for arc in outmap_det.arcs(state):

                ch = syms.find(arc.olabel)
                w = float(arc.weight)
                print ch, w
                arc_dict[ch] = w

        # form a vector
        eegvec = []
        for (key, val) in arc_dict.iteritems():
            eegvec.append((key,val))


        # is normalized
        eegvec = sorted(eegvec, key=lambda prior: prior[1])

        return eegvec

def sum_up(pr_vector1, pr_vector2):
    '''
    INPUT:
       two vectors of symbol distribution. Each cell
       is of the form (symbol, float)
    OUTPUT:
       one vector with the coresponding sum over shared
       symbols.
    '''
    dist1 = sorted(pr_vector1, key=lambda symbol: symbol[0])
    dist2 = sorted(pr_vector2, key=lambda symbol: symbol[0])
    dist1_2 = [(s1, p1+p2) for ((s1, p1),(s2, p2)) in zip(dist1,dist2)]
    print dist1
    print dist2
    print dist1_2
    return sorted(dist1_2, key=lambda symbol: symbol[1])

def min_symbol(dist):
    return dist[0][0]

def error_rate(ref, pred):
    '''
    INPUT:
       two strings
    DISPLAY:
       error rate
    '''
    errs = [s1 for s1, s2 in zip(ref[1:],pred[1:]) if s1 != s2]
    #print "The sentence ",ref, " had ", len(errs) / len(ref[1:]) * 100, "% error rate"
    return len(errs)
