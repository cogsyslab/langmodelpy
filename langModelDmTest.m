% langModelDmTest
% does a few quick things to ensure we can drive this object from matlab

clearvars; clc;

% you should have python installed
pyversion

% adds current folder to MATLAB's python search path (kludge: current
% folder must contain langModelMod)
if count(py.sys.path,'') == 0
    insert(py.sys.path,int32(0),'');
end

langModelObj = py.langModelMod.langModelDm;

numSym = 28;
numChar = 3;
symProb = rand(numSym, numChar);
symProb = bsxfun(@times, symProb, (1 ./ sum(symProb)));

langModelObj.reset;
langModelObj.addSym(toggleNumpy(symProb));
symNextProb = toggleNumpy(langModelObj.querySym());

assert(isequal(symProb(:, end), symNextProb), ...
    'lost symProb in langModelDmObj somewhere');