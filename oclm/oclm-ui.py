from __future__ import division
from oclm import *
from itertools import groupby
import random
import argparse, time, curses
from ascii_graph import Pyasciigraph
import locale
import pywrapfst as fst
from ebitweight import BitWeight
from eeg_utils import sausage_generator, eegs, generate_eeg
import math
import Levenshtein
import time

def display(main_win, args):
    # Sets up the color
    curses.start_color()
    curses.use_default_colors()
    for i in range(0, curses.COLORS):
        curses.init_pair(i+1, i, -1)
    graph = Pyasciigraph(force_max_value=1.0, float_format='{0:,.2f}')

    # Creates four windows for display
    topk_win = curses.newwin(13, 80, 2, 84)
    #post_win = curses.newwin(30, 80, 16, 3)
    lm_win = curses.newwin(30, 80, 16, 84)
    #eeg_win = curses.newwin(30, 80, 16, 165)
    eeg_win = curses.newwin(30, 80, 16, 3)

    sym = fst.SymbolTable.read(args.machines + '/ch_syms.bin')
    lm = oclm(args)
    eeg_samples = eegs(args.eeg)
    predict_str = ""
    i = 0
    main_win.addstr(0, 0, "Current mode: Copy task (ESC to quit)", curses.A_REVERSE)
    while True:
        main_win.addstr(8,  3, "COPY:     " + args.sent)
        main_win.addstr(9,  3, "HISTORY:  " + args.sent[:i], curses.color_pair(2))
        main_win.addstr(10, 3, "OUTPUT:   " + predict_str + '-' * (len(args.sent) - i))
        main_win.refresh()
        #
        # Gets the distribution of next character
        #
        if i < len(args.sent):
            topk_wds = []
            '''
            # Generates EEG sausage.
            eeg_saus = sausage_generator(args.sent[:i], sym, args.eeg)
            # construct word history
            wrd_seq = lm.word_sequence_history(eeg_saus)
            # add sigma
            word_sigma = lm.add_sigma(wrd_seq)
            # take topk and form a character rep
            ltr_topk = lm.topk_choice(word_sigma, topk_wds)
            # union with prefix LM
            united_LM = lm.union_LMs(ltr_topk)
            # Extracts trailing characters.
            trailing_chars = lm.extract_trailing_chars(eeg_saus)
            # Restricts the length.
            trailing_chars = lm.restrict_chars_length(trailing_chars)
            # Gets the distribution over next character.
            priors = lm.next_char_dist(trailing_chars, united_LM)
            '''
            start_time = time.time()
            ch = args.sent[i]
            if ch == ' ' or ch == '_':
                ch = '#'
            eeg_dist = generate_eeg(eeg_samples, ch)
            nbest_eeg_dist = eeg_dist[:args.nbest]
            # Normalizes the EEG evidence.
            '''
            tmp_dst = [BitWeight(pr) for _,pr in nbest_eeg_dist]
            total = sum(tmp_dst, BitWeight(1e6))
            norm_tmp_dst = [(pr / total).loge() for pr in tmp_dst]
            nbest_eeg_dist = zip([ch for ch,_ in nbest_eeg_dist], norm_tmp_dst)
            '''
            if args.old:
                history_chars = lm.separate_sausage(lm.history_fst, lm.ch_before_last_space_fst)
                trailing_chars = lm.separate_sausage(lm.history_fst, lm.ch_after_last_space_fst)

                word_lattice = fst.compose(history_chars, lm.ltr2wrd)
                #word_lattice = fst.push(word_lattice, push_weights=True, to_final=True)
                word_lattice.project(project_output=True).rmepsilon()
                word_lattice = fst.determinize(word_lattice)
                word_lattice.minimize().topsort()
                if word_lattice.num_states() == 0:
                    word_lattice = create_empty_fst(lm.wd_syms, lm.wd_syms)

                trailing_chars_sigma = lm.add_char_selfloop(trailing_chars, sym)
                trailing_chars_sigma.arcsort(st="olabel")
                current_words = fst.compose(trailing_chars_sigma, lm.no_phi_lex)
                current_words.project(project_output=True).rmepsilon()
                current_words = fst.determinize(current_words)
                current_words.minimize().topsort

                word_seq = word_lattice.copy().concat(current_words).rmepsilon()
                topk_wds = []
                topk = lm.topk_choice(word_seq)
                united_LM = lm.combine_ch_lm(topk, topk_wds)
                priors = lm.next_char_dist(trailing_chars, united_LM)
                execution_time = time.time() - start_time
                lm.append_eeg_evidence(nbest_eeg_dist)
            else:
                priors = lm.predict(topk_wds)
                #predict_str += priors[0][0] if priors[0][0] != '#' else ' '
                execution_time = time.time() - start_time
                lm.update(nbest_eeg_dist)


            main_win.addstr(15, 3, "execution time: {0:.2f}s".format(execution_time).encode('UTF-8'))
        else:
            ch_distance = Levenshtein.distance(predict_str, args.sent)
            main_win.addstr(12, 3,
                    "Character edit distance: {0}({1:.1f}% error rate)".format(
                        ch_distance, ch_distance / float(len(args.sent)) * 100))
            main_win.refresh()

        #
        # Display the histograms.
        # (TODO) Add EEG and posterior histograms.
        #

        # Normalize the topk words distribution
        '''
        topk_wds = sorted(topk_wds, key=lambda(x,y):x)
        hist = []
        for k, g in groupby(topk_wds, lambda(x,y):x):
            hist.append((k, sum([p for _,p in g])))
        denom = sum([p for _,p in hist])
        topk_wds = sorted([(wd,p/denom) for wd,p in hist], key=lambda(x,y):-y)
        '''

        # Display the choice
        topk_hist = [(wd, prob) for wd, prob in topk_wds]
        for j, line in enumerate(graph.graph("Top-10 Words", topk_hist)):
            if j >= 12: break
            topk_win.addstr(j+1, 0, line.encode('UTF-8'))
        topk_win.refresh()


        eeg_hist = sorted([(ch, math.e**(-prob)) for ch, prob in nbest_eeg_dist],
                          key=lambda x:x[0])
        for j, line in enumerate(graph.graph("EEG Evidence", eeg_hist)):
            # top 2 lines for head
            if (j >= 2 and i < len(args.sent)) and \
                (eeg_hist[j-2][0] == args.sent[i] or
                 (eeg_hist[j-2][0] == '#' and args.sent[i] == ' ')):
                eeg_win.addstr(j+1, 0, line.encode('UTF-8'), curses.color_pair(5))
            else:
                eeg_win.addstr(j+1, 0, line.encode('UTF-8'))
        eeg_win.refresh()


        priors_hist = sorted([(ch, math.e**(-prob)) for ch, prob in priors],
                             key=lambda x:x[0])
        for j, line in enumerate(graph.graph("Language Model Priors", priors_hist)):
            # top 2 lines for head
            if (j >= 2 and i < len(args.sent)) and \
                (priors_hist[j-2][0] == args.sent[i] or
                 (priors_hist[j-2][0] == '#' and args.sent[i] == ' ')):
                lm_win.addstr(j+1, 0, line.encode('UTF-8'), curses.color_pair(5))
            else:
                lm_win.addstr(j+1, 0, line.encode('UTF-8'))
        lm_win.refresh()

        # Computes the posteriors to predict the next character.
        post_hist = []
        nbest_eeg_dist.sort(key=lambda x:x[0])
        priors.sort(key=lambda x:x[0])
        e_i = 0
        for k in xrange(len(priors)):
            if e_i < len(nbest_eeg_dist) and nbest_eeg_dist[e_i][0] == priors[k][0]:
                post_hist.append((priors[k][0], nbest_eeg_dist[e_i][1] + priors[k][1]))
                e_i += 1
            else:
                post_hist.append((priors[k][0], priors[k][1]))
        post_hist.sort(key=lambda x:x[1])
        if i < len(args.sent):
            predict_str += post_hist[0][0] if post_hist[0][0] != '#' else ' '

        pressed_ch = main_win.getch()
        if pressed_ch == 27: break  # Exit if pressed "ESC"
        i += 1

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--sent', dest='sent', type=str,
        help="the string for copy task")
    parser.add_argument('--eeg', dest='eeg', type=str,
        help='the eeg samples file')
    parser.add_argument('-d', '--deterministic', action="store_true",
        help='whether to use eeg file to noisify evidence')
    parser.add_argument('-n', '--nbest', type=int, default=3,
        help='Chooses nbest eeg samples for simulation')
    parser.add_argument('--specializer', action='store_true')
    parser.add_argument('--old', action='store_true')
    parser.add_argument('--machines', help='folders containing all predefined fsts.')
    parser.add_argument('--factor', type=float, default=0.5, help='how much factor we put on topk words.')
    random.seed(1437)
    args = parser.parse_args()
    locale.setlocale(locale.LC_ALL, '')
    curses.wrapper(display, args)
