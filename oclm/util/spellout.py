import pywrapfst as fst
import argparse, random

"""spellout out machine traslates words back to letter sequences
some words may contain symbols the are not found in the letter symbol set
therefore they are further parsed to map them to the letter symbol set"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--input', type=str,
        help='Input word list')
    parser.add_argument('--isym', type=str,
        help='Input Symbol Table')
    parser.add_argument('--osym', type=str,
        help='Output Symbol Table')
    parser.add_argument('outdir', help='Output directory')
    args = parser.parse_args()

    isym = fst.SymbolTable.read_text(args.isym)
    osym = fst.SymbolTable.read_text(args.osym)

    letter = fst.Fst()
    letter.set_input_symbols(isym)
    letter.set_output_symbols(osym)
    letter.add_state()
    letter.set_start(0)

    for word in open(args.input, "r").readlines():
        word = word.split("\t")[0].strip()
        if word.startswith('<'):
            continue
        nletter = fst.Fst()
        nletter.set_input_symbols(isym)
        nletter.set_output_symbols(osym)
        nletter.add_state()
        for i,ltr in enumerate(word):
            nletter.add_state()
            code2 = osym.find(ltr)
            if i==0:
                nletter.set_start(0)
                code1 = isym.find(word)
                nletter.add_arc(i, fst.Arc(code1, code2, None, i+1))
            else:
                code1 = isym.find("<epsilon>")
                nletter.add_arc(i, fst.Arc(code1, code2, None, i+1))
        i += 1
        code1 = isym.find("<epsilon>")
        code2 = osym.find("#")
        nletter.add_arc(i, fst.Arc(code1, code2, None, i+1))
        nletter.add_state()
        nletter.set_final(i+1)
        letter.union(nletter)
    letter.rmepsilon()
    letter.write(args.outdir + "spellout.fst")
