#!/bin/bash
#set -x
export LD_LIBRARY_PATH=/usr/local/lib/fst
file=$1
fstspecial --fst_type=sigma --sigma_fst_sigma_label=65535 --sigma_fst_rewrite_mode="always" $file > new_wrd_seq.fst
