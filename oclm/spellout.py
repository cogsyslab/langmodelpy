import pywrapfst as fst

"""spellout out machine traslates words back to letter sequences
some words may contain symbols the are not found in the letter symbol set
therefore they are further parsed to map them to the letter symbol set"""

def dig2word(w):
    new_w = ""
    for ltr in w:
        if ltr.isdigit():
            if ltr == "0":
                new_w+="#oh"
            elif ltr == "1":
                new_w+="#one"
            elif ltr == "2":
                new_w+="#two"
            elif ltr == "3":
                new_w+="#three"
            elif ltr == "4":
                new_w+="#four"
            elif ltr == "5":
                new_w+="#five"
            elif ltr == "6":
                new_w+="#six"
            elif ltr == "7":
                new_w+="#seven"
            elif ltr == "8":
                new_w+="#eight"
            elif ltr == "9":
                new_w+="#nine"
        else:
            new_w+=ltr
    return new_w

def spellout_machine(orig_machine):
    s_in = orig_machine.output_symbols()
    ltr_lm_path = "machines/ltr_lm.fst"
    lm = fst.Fst.read(ltr_lm_path)
    s_out = lm.input_symbols()
    
    #syms_out = "../sausage/phi_ltr.syms"
    #for line in open(syms_out,"r"):
     #   symbol, code = line.split()
      #  s_out.add_symbol(symbol)

    letter = fst.Fst()
    letter.set_input_symbols(s_in)
    letter.set_output_symbols(s_out)
    letter.add_state()

    filename = "gen_machine/spellout/words"
    for word in open(filename, "r").readlines():
        word = word.split("\t")[0].strip()
        orig = word
        word = dig2word(word)
        nletter = fst.Fst()
        nletter.set_input_symbols(s_in)
        nletter.set_output_symbols(s_out)
        nletter.add_state()
        for i,ltr in enumerate(word):
            nletter.add_state()
            code2 = s_out.find(ltr)
            if i==0:
                nletter.set_start(0)
                code1 = s_in.find(orig)
                nletter.add_arc(i, fst.Arc(code1, code2, None, i+1))
            else:
                code1 = s_in.find("<epsilon>")
                nletter.add_arc(i, fst.Arc(code1, code2, None, i+1))
        nletter.set_final(i+1)
        letter.union(nletter)
    letter.rmepsilon()
    letter.write("machines/spellout.fst")




