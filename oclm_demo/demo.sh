#!/bin/bash

set -x
NBEST=3
SENT="the#former#coachman#and#delphine#were#gaining#on#her#as#she#raced#down#perdido#street"
#SENT="xoxo#be#good"
#pushd new_oclm
  
export LD_LIBRARY_PATH=/usr/local/lib/fst
python demo_oclm.py --sent $SENT --eeg /Users/dudy/CSLU/bci/langmodelpy/lm/EEGData/EEGEvidence.txt-high --nbest $NBEST --specializer --machines machines --old --log doc_demo --test
