import numpy as np


class langModelDm:
    """ dummy language model class

    demos how MATLAB and python play nice together, this dummy class is
    intended to be replaced by a functional language model down the road

    attributes:
        contextProb = [numSym x numContext] array, where element i, j describes
                      the probability that the j-th element in the target
                      string is the i-th possible symbol (e.g. sym in
                      {a,b,c, ..., x,y,z,_,<} )
    """

    def __init__(self):
        self.contextProb = np.array([])

    def reset(self):
        """ resets context """
        self.contextProb = np.zeros((0, 0))

    def addSym(self, symProb):
        """ adds symbols to context """

        numSym = self.contextProb.shape[0]
        if numSym:
            # current context exists
            assert symProb.shape[0] is numSym
            self.contextProb = np.concatenate((self.contextProb, symProb), 1)
        else:
            # no current context
            self.contextProb = symProb

    def querySym(self):
        """ queries prob of next symbol """

        # dummy
        numContext = self.contextProb.shape[1]
        if numContext:
            # if any context is available, return latest context distribution
            # its nonsense, but nonsesnse we can validate in MATLAB
            symProb = self.contextProb[:, -1]
        else:
            # if no context is available, return uniform
            numSym = 28
            symProb = np.ones((numSym, 1)) / numSym

        return symProb

if __name__ == '__main__':

    numSym = 28
    numChar = 3
    symProb = np.zeros((numSym, numChar))

    langModelDmObj = langModelDm()
    langModelDmObj.addSym(symProb)
    symNextProb = langModelDmObj.querySym()
