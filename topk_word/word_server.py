import argparse
import pywrapfst as fst
import shlex, subprocess
import time

def gen_sent(sentence):
  """ generate sigma machine from sentence
      sigma_machine.fst is the output""" 

  # generate a txt file
  fname = "sigma_machine"
  f = open(fname+".txt", "w")
  sentence.append("SIGMA")
  for i, word in enumerate(sentence):
    states = str(i)+" "+str(i+1)+" "+word+" "+word
    f.write(states)
    f.write("\n")
  f.write(str(i+1))
  f.close()

  # generate sigma fst file
  command_line = "./fst_gen.sh "+fname+".txt"
  args = shlex.split(command_line)
  p = subprocess.Popen(args, stdout=subprocess.PIPE)
  output = p.communicate()[0]
  fname+=".fst"
  outfst = fst.Fst.read(fname)
  print outfst
  return outfst

def display(outfst, query):
  # present the topk selections
  dist = []
  syms = outfst.input_symbols()
  state = outfst.start()   
  for arc in outfst.arcs(state):  
    label = syms.find(arc.ilabel)
    pr = float(arc.weight)
    dist.append((label, pr))
  sort_word = sorted(dist, key=lambda prior: prior[1])
  print "Query: "," ".join(query)
  for (label, pr) in sort_word:
    print "{0:20} {1:.3f}".format(label, pr)


def topk_choice(lm, sent, rfn):
  """extracts the topk choices of lm given a sentence
     input: lm.fst and sentence string
     output: display of topk for each history"""

  sent = sent.split("_")
  lm = fst.Fst.read(lm)
  refiner = fst.Fst.read(rfn)
  for i in range(len(sent)+1):
    # generate sentence fst    
    query = gen_sent(sent[:i])
    fstout = fst.intersect(lm, query)
    fst_det = fst.determinize(fstout)
    fst_p = fst.push(fst_det, push_weights=True, to_final=True)
    fst_rm = fst.rmepsilon(fst_p)
    short = fst.shortestpath(fst_rm, nshortest=20)
    short_det = fst.determinize(short)
    short_rm = fst.rmepsilon(short_det)
    two_state = fst.compose(short_rm, refiner)
    output = two_state.project(project_output=True)
    output = fst.rmepsilon(output)
    output.minimize()
    # traverse and display
    display(output, sent[:i])

if __name__ == "__main__":    
    parser = argparse.ArgumentParser()
    parser.add_argument('--lm', dest='lm', type=str,
        help='path to the language model (fst)')
    parser.add_argument('--sent', dest='sent', type=str,
        help='the query (underlined seperated words)')
    parser.add_argument('--rfn', dest='rfn', type=str,
        help='the refiner (fst)')
    args = parser.parse_args()
    topk_choice(args.lm, args.sent, args.rfn) 
