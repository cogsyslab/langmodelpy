#!/bin/bash
set -x
export LD_LIBRARY_PATH=/usr/local/lib/fst
file=$1
fstcompile --isymbols=../sigma/syms.txt --osymbols=../sigma/syms.txt --keep_isymbols --keep_osymbols $file | fstspecial --fst_type=sigma --sigma_fst_sigma_label=65535 --sigma_fst_rewrite_mode="always" > sigma_machine.fst
