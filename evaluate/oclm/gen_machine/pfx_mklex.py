#! /usr/bin/env python

import sys

def dig2word(w):
  new_w = ""
  for ltr in w:
    if ltr.isdigit():
        if ltr == "0":
          new_w+="ohd"
        elif ltr == "1":
          new_w+="oned"
        elif ltr == "2":
          new_w+="twod"
        elif ltr == "3":
          new_w+="threed"
        elif ltr == "4":
          new_w+="fourd"
        elif ltr == "5":
          new_w+="fived"
        elif ltr == "6":
          new_w+="sixd"
        elif ltr == "7":
          new_w+="sevend"
        elif ltr == "8":
          new_w+="eightd"
        elif ltr == "9":
          new_w+="nined"
    else:
      new_w+=ltr
  return new_w

eps = '<epsilon>'
spa = '#'
phi = '<phi>'
rho = '<rho>'
unk = '<unk>'
cost = 1
l_cost = 1
# the cost is high to call it a prefix, you should prefer using # one you get one
printable = 'abcdefghijklmnopqrstuvwxyz'

def make_lexicon(ifile):
    s = 2
    for line in ifile:
        orig_word = line.split()[0]
        word = dig2word(orig_word)
        word = word.strip()
        if word.startswith('<'):
            continue  # special
        for i, letter in enumerate(word):
            if i == 0:
                print 0, s, letter, eps
            else:
                print s, s + 1, letter, eps
                s = s + 1
            print s, 1, phi, eps, cost  # unknown tokens will match
        #print s, 1, phi, eps, cost      # non-consuming phi symbols
        print s, 0, spa, orig_word           # whitespace required (even at the end)
        s = s + 1
    print 0, 1, phi, eps, cost          # first letter does not match any word
    for letter in list(printable):
        print 1, 1, letter, eps, l_cost
    print 1, 0, spa, unk                # whitespace required (even at the end)
    print 0
    print 1
if __name__ == '__main__':
    make_lexicon(sys.stdin)

