#!/bin/bash
set -x
export LD_LIBRARY_PATH=/usr/local/lib/fst
file=$1
syms=$2
special=$3
echo -e "0\t$special" > phi_relabel.txt
farcompilestrings -symbols=${syms} -keep_symbols=1 --unknown_symbol="<unk>" $file > input.far
ngramcount -order=2 input.far > input.counts
ngrammake input.counts > input.mod
fstrelabel --relabel_ipairs=phi_relabel.txt --relabel_opairs=phi_relabel.txt input.mod > input.relabeld
fstarcsort input.relabeld > input.sorted.rlbl 
fstspecial --fst_type=phi --phi_fst_phi_label=$special input.sorted.rlbl > machines/wrd_lm.fst
rm input.far
rm input.counts
rm input.mod
rm input.relabeld
rm input.sorted.rlbl
rm phi_relabel.txt

