#!/bin/bash
set -x
export LD_LIBRARY_PATH=/usr/local/lib/fst
syms=$1
special=$2
echo -e "0\t0\t<unk>\t<epsilon>\n0\t0\t<rho>\t<rho>\n0" > rho.txt
fstcompile --isymbols=${syms} --osymbols=${syms} --keep_isymbols --keep_osymbols rho.txt | fstspecial --fst_type=rho --rho_fst_rho_label=$special --rho_fst_rewrite_mode="always" > machines/unk2eps.fst
rm rho.txt
