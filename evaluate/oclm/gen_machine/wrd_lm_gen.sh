#!/bin/bash
crp=$1
ord=5
syms=$2
farcompilestrings -symbols=${syms} -keep_symbols=1 $crp >$crp.far
ngramcount --backoff_label=0 -order=$ord $crp.far >$crp.cnts
ngrammake --method=kneser_ney $crp.cnts machines/wrd_lm.fst
rm $crp.far
rm $crp.cnts

