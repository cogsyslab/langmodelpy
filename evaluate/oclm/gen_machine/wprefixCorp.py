import sys

def prefix_print(datafile, wrd_lst):
  vocab = []
  for word in open(wrd_lst, 'r').readlines():
    vocab.append(word.strip())
  vocab = set(sorted(vocab))
  for g, line in enumerate(open(datafile, 'r').readlines()):
    line = line.strip()
    line = line.split()
    new_line = []
    for word in line:
      # ensure word[i] is valid else continue
      if word in vocab:
        new_line.append(word)
    for i in range(1,len(new_line)+1):
      print " ".join(new_line[:i])

if __name__ == "__main__":    
  prefix_print(sys.argv[1], sys.argv[2])
    

