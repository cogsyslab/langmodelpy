#!/bin/bash
set -x
export LD_LIBRARY_PATH=/usr/local/lib/fst
file=$1
symsw=$2
symsc=$3
special=$4
fstcompile --isymbols=${symsc} --osymbols=${symsw} --keep_isymbols --keep_osymbols $file | fstdeterminize | fstminimize | fstarcsort | fstspecial --fst_type=phi --phi_fst_phi_label=$special --phi_fst_rewrite_mode="always" > machines/ltr2wrd_phi.fst
rm $file
