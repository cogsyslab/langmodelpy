#!/bin/bash
set -x
export LD_LIBRARY_PATH=/usr/local/lib/fst
syms=$1
special=$2

echo -e "0\t1\t<sigma>\t<epsilon>" > sig.txt

for i in 1 2 3 4 5 6 7 8 9 10 11
do
  echo -e "${i}\t$(($i+1))\t<sigma>\t<sigma>" >> sig.txt
done
echo -e "$(($i+1))" >> sig.txt

fstcompile --isymbols=${syms} --osymbols=${syms} --keep_isymbols --keep_osymbols sig.txt | fstspecial --fst_type=sigma --sigma_fst_sigma_label=$special --sigma_fst_rewrite_mode="always" > machines/length_machine.sigma.fst
rm sig.txt
