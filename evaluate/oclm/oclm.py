from __future__ import division
import argparse
import pywrapfst as fst
import shlex, subprocess
import math
from ebitweight import BitWeight
from eeg_utils import sausage_generator
import sys
import time
import Levenshtein
import io
import logging
import numpy as np
import json
import math
import specializer

#python online_context_lm.py --sent hello#here

class oclm:
    def __init__(self):
        self.spell = fst.Fst.read("machines/spellout.fst")
        self.prefix_lm = fst.Fst.read("machines/ltr_lm.fst")
        self.refiner = fst.Fst.read("machines/refiner.fst")
        self.lm = fst.Fst.read("machines/wrd_lm.fst")
        self.sig_mcn = fst.Fst.read("machines/sigma_machine_words.fst")
        self.ltr2wrd = fst.Fst.read("machines/ltr2wrd_phi.fst")
        self.ch_after_last_space_fst = fst.Fst.read("machines/ch_after_last_space.sigma.fst")
        self.length_fst = fst.Fst.read("machines/length_machine.sigma.fst")
        self.ltr_dist = fst.Fst.read("machines/ltr_dist.sigma.fst")
        self.unk2epsi = fst.Fst.read("machines/unk2eps.fst")
        self.num_states = sum(1 for state in self.length_fst.states()) # should have the below funtionality working
        self. sigma_label = 65535
        self.sigma_mode = "always"

    def normalize(self, anfst):
        """"produce a normalized fst"""
        # possibly there's a shorter way
        # that keeps all in fst land
        dist = []
        labels = []
        syms = anfst.input_symbols()
        state = anfst.start()
        flag = 0
        for arc in anfst.arcs(state):
            label = syms.find(arc.ilabel)
            pr = float(arc.weight)
            dist.append(BitWeight(pr)) # ebitweight gets -log(pr) only
            labels.append(label)
            if not flag: # init sum with fist value
                sum_value = BitWeight(pr)
                flag = 1
        # normalize distribution
        for value in dist[1:]:
            sum_value+=value # will sum in log domain (log-add)
        norm_dist = [(prob/sum_value).loge() for prob in dist]
        del anfst
        # construct a norm fst
        output = fst.Fst()
        output.set_input_symbols(syms)
        output.set_output_symbols(syms)
        output.add_state()
        output.add_state()
        for (pr, label) in zip(norm_dist,labels):
            code = syms.find(label)
            output.add_arc(0, fst.Arc(code, code, pr, 1))
        output.set_start(0)
        output.set_final(1)
        return output

    def wrd2ltr(self, fstout):
        """generate a letter fst with the topk
        words and thier corresponding weights """

        # if there are normalization methods in fst land..
        norm_fst = self.normalize(fstout)
        s_out = fstout.input_symbols()
        letter = fst.compose(fstout, self.spell)
        letter.push(to_final=False)
        letter.project(project_output=True)
        letter = fst.rmepsilon(letter)
        for state in letter.states():
            if state==0:
                continue
            letter.set_final(state)
        return letter

    def union_LMs(self, new_lm):
        new_lm.union(self.prefix_lm)
        return new_lm

    def topk_choice(self, word_sequence):
        """extracts the topk choices of
        lm given a word history (lattice)
        input: lm.fst and sentence string
        output: topk words to complete the lattice"""
        # generate sentence fst
        fstout = fst.intersect(word_sequence, self.lm)
        fst_det = fst.determinize(fstout)
        fst_p = fst.push(fst_det, push_weights=True, to_final=True)
        fst_rm = fst.rmepsilon(fst_p)
        short = fst.shortestpath(fst_rm, nshortest=20)
        short_det = fst.determinize(short)
        short_rm = fst.rmepsilon(short_det)
        two_state = fst.compose(short_rm, self.refiner)
        output = two_state.project(project_output=True)
        output = fst.rmepsilon(output)
        output = fst.determinize(output)
        output.minimize()
        return self.wrd2ltr(output)

    def add_sigma(self, fst_machine):
        # soon to be replaced
        # with a python operation
        fst_machine.concat(self.sig_mcn)
        fst_machine.rmepsilon()
        fst_machine.arcsort(st="olabel")
        specialed = specializer.sigma(fst_machine, self.sigma_label, self.sigma_mode).get()
        return specialed

    def word_sequence_history(self, eeg_saus):
        """generate a probable word
        sequence given the EEG samples
        by intersecting it with word
        language model"""

        word_seq = fst.compose(eeg_saus, self.ltr2wrd)
        fst.push(word_seq, push_weights=True, to_final=True)
        word_seq.project(project_output=True)
        word_seq.rmepsilon()
        word_seq = fst.compose(word_seq, self.unk2epsi)
        word_seq.project(project_output=True)
        return word_seq
        # the motivation to have a function is to
        # free the memory from ltr2wrd once the
        # intersection is done

    def extract_trailing_chars(self, sausage):
        '''
        Extract the trailing characters after the last space,
        or from the beginning if there is no space.
        '''
        trailing_chars = fst.compose(sausage, self.ch_after_last_space_fst)
        trailing_chars.project(True)
        trailing_chars.rmepsilon()
        trailing_chars = fst.determinize(trailing_chars)
        trailing_chars.minimize()
        return trailing_chars

    def restrict_chars_length(self, trailing_chars):
        '''
        Restrict the length of trailing characters to avoid complex computation.
        The exact number of length depending on the implementation of fst.
        '''
        num_states = trailing_chars.num_states()
        if num_states > self.num_states:
            restricted_length = fst.compose(trailing_chars, self.length_fst)
            restricted_length.project(project_output=True)
            restricted_length.rmepsilon()
            restricted_length = fst.determinize(restricted_length)
            restricted_length.minimize()
            return restricted_length
        else:
            return trailing_chars

    def concat_alphabet(self, ifst):
        '''
        Concatenate all characters in the alphabet to the end of a given fst.
        '''
        ofst = ifst.copy()
        sigma = fst.Fst()
        sigma.set_input_symbols(ofst.input_symbols())
        sigma.set_output_symbols(ofst.output_symbols())
        sigma.add_state()
        sigma.set_start(0)
        for code, ch in ofst.input_symbols():
            if ch.startswith("<"):  # Don't include special arcs in sigma machine.
                continue
            sigma.add_arc(0, fst.Arc(code, code, None, 1))
        sigma.add_state()
        sigma.set_final(1)
        ofst.concat(sigma)
        ofst.rmepsilon()
        return ofst

    def next_char_dist(self, history, char_lm):
        '''
        Get the distribution of next character.
        '''
        history = self.concat_alphabet(history)
        history.arcsort(st="olabel")
        output = fst.intersect(history, char_lm)
        output.rmepsilon()
        output = fst.determinize(output)
        output.minimize()

        # reads an fst to combine the weights of the next character.
        last_ltr = fst.compose(output, self.ltr_dist)
        last_ltr.project(True)
        last_ltr.push(to_final=True)
        last_ltr.rmepsilon()
        last_ltr = fst.determinize(last_ltr)
        last_ltr.minimize()
        # Extracts priors. Although it's a two-state machine, we have the
        # generic traverse procedure here just in case.
        prev_stateid = curr_stateid = None
        for state in last_ltr.states():
            if not curr_stateid is None:
                prev_stateid = curr_stateid
            curr_stateid = state
        priors = []
        syms = last_ltr.input_symbols()
        for arc in last_ltr.arcs(prev_stateid):
            ch = syms.find(arc.ilabel)
            w = float(arc.weight)
            if len(ch) == 1:
                priors.append((ch, w))

        # Sorts the prior by the probability and normalize it.
        priors = sorted(priors, key=lambda prior: prior[1])
        priors_vals = [BitWeight(prob) for _,prob in priors]
        total = sum(priors_vals, BitWeight(1e6))
        norm_priors = [(prob / total).loge() for prob in priors_vals]
        return zip([ch for ch,_ in priors], norm_priors)

def pipeline_performance(args):
    '''
    Use the commandline arguments to measure the performance of our pipeline.
    '''
    path = "/Users/dudy/CSLU/bci/langmodelpy/oclm/"
    sym = fst.SymbolTable.read(path+'machines/ch_syms.bin')
    lm = oclm()

    # Timer
    eeg_saus_times = []
    wrd_seq_times = []
    word_sigma_times = []
    ltr_topk_times = []
    united_LM_times = []
    trailing_chars_times = []
    restricted_trailing_chars_times = []
    priors_times = []
    total_times = []

    # Main Process
    predict = ""
    for i in xrange(len(args.sent)):
        start_time = time.time()
        print "==================================================================="
        print "Current history:", args.sent[:i] + "_" * (len(args.sent) - i)
        # Generates the eeg sausage depending on the sentence
        eeg_saus = sausage_generator(args.sent[:i], sym, args.eeg)
        eeg_saus_time = time.time()
        eeg_saus_times.append(eeg_saus_time - start_time)
        # construct word history
        wrd_seq = lm.word_sequence_history(eeg_saus)
        wrd_seq_time = time.time()
        wrd_seq_times.append(wrd_seq_time - eeg_saus_time)
        #spellout_machine(wrd_seq) # to generate spellout machine
        # add sigma
        word_sigma = lm.add_sigma(wrd_seq)
        word_sigma_time = time.time()
        word_sigma_times.append(word_sigma_time - wrd_seq_time)
        # take topk and form a character rep
        ltr_topk = lm.topk_choice(word_sigma)
        ltr_topk_time = time.time()
        ltr_topk_times.append(ltr_topk_time - word_sigma_time)
        # union with prefix LM
        united_LM = lm.union_LMs(ltr_topk)
        united_LM_time = time.time()
        united_LM_times.append(united_LM_time - ltr_topk_time)
        # Extracts trailing characters.
        trailing_chars = lm.extract_trailing_chars(eeg_saus)
        trailing_chars_time = time.time()
        trailing_chars_times.append(trailing_chars_time - united_LM_time)
        # Restricts the length.
        trailing_chars = lm.restrict_chars_length(trailing_chars)
        restricted_trailing_chars_time = time.time()
        restricted_trailing_chars_times.append(restricted_trailing_chars_time - trailing_chars_time)
        # Gets the distribution over next character.
        priors = lm.next_char_dist(trailing_chars, united_LM)
        priors_time = time.time()
        priors_times.append(priors_time - restricted_trailing_chars_time)
        total_times.append(priors_time - start_time)
        # Appends the most possible character to prediction
        predict += priors[0][0]
        # Outputs the distribution
        for ch, p in priors:
            if ch == args.sent[i] or (args.sent[i] == ' ' and ch == '#'):
                print "\033[94m\033[1m{0}\t{1:.3f}\033[0;0m".format(ch, p)
            else:
                print "{0}\t{1:.3f}".format(ch, p)
        print "==================================================================="

    # Outputs the performance stats.
    print "==================================================================="
    print "Total time: {0:.3f}s, Avg: {1:.3f}s, Min: {2:.3f}s ({3} chars), Max: {4:.3f}s ({5} chars)".format(
            sum(total_times), sum(total_times)/float(len(total_times)), min(total_times),
            total_times.index(min(total_times)), max(total_times), total_times.index(max(total_times)))
    print "  Create EEG sausage: {0:.3f}s ({1:.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4} chars), Max: {5:.3f}s ({6} chars)".format(
            sum(eeg_saus_times), sum(eeg_saus_times)/sum(total_times)*100,
            sum(eeg_saus_times)/len(eeg_saus_times),
            min(eeg_saus_times), eeg_saus_times.index(min(eeg_saus_times)),
            max(eeg_saus_times), eeg_saus_times.index(max(eeg_saus_times)))
    print "  Generate word sequence history: {0:.3f}s ({1:.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4} chars), Max: {5:.3f}s ({6} chars)".format(
            sum(wrd_seq_times), sum(wrd_seq_times)/sum(total_times)*100,
            sum(wrd_seq_times)/len(wrd_seq_times),
            min(wrd_seq_times), wrd_seq_times.index(min(wrd_seq_times)),
            max(wrd_seq_times), wrd_seq_times.index(max(wrd_seq_times)))
    print "  Add Sigma: {0:.3f}s ({1:.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4} chars), Max: {5:.3f}s ({6} chars)".format(
            sum(word_sigma_times), sum(word_sigma_times)/sum(total_times)*100,
            sum(word_sigma_times)/len(word_sigma_times),
            min(word_sigma_times), word_sigma_times.index(min(word_sigma_times)),
            max(word_sigma_times), word_sigma_times.index(max(word_sigma_times)))
    print "  Get topK words: {0:.3f}s ({1:.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4} chars), Max: {5:.3f}s ({6} chars)".format(
            sum(ltr_topk_times), sum(ltr_topk_times)/sum(total_times)*100,
            sum(ltr_topk_times)/len(ltr_topk_times),
            min(ltr_topk_times), ltr_topk_times.index(min(ltr_topk_times)),
            max(ltr_topk_times), ltr_topk_times.index(max(ltr_topk_times)))
    print "  Get prefix character LM: {0:.3f}s ({1:.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4} chars), Max: {5:.3f}s ({6} chars)".format(
            sum(united_LM_times), sum(united_LM_times)/sum(total_times)*100,
            sum(united_LM_times)/len(united_LM_times),
            min(united_LM_times), united_LM_times.index(min(united_LM_times)),
            max(united_LM_times), united_LM_times.index(max(united_LM_times)))
    print "  Extract trailing characters: {0:.3f}s ({1:.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4} chars), Max: {5:.3f}s ({6} chars)".format(
            sum(trailing_chars_times), sum(trailing_chars_times)/sum(total_times)*100,
            sum(trailing_chars_times)/len(trailing_chars_times),
            min(trailing_chars_times), trailing_chars_times.index(min(trailing_chars_times)),
            max(trailing_chars_times), trailing_chars_times.index(max(trailing_chars_times)))
    print "  Restrict length: {0:.3f}s ({1:.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4} chars), Max: {5:.3f}s ({6} chars)".format(
            sum(restricted_trailing_chars_times), sum(restricted_trailing_chars_times)/sum(total_times)*100,
            sum(restricted_trailing_chars_times)/len(restricted_trailing_chars_times),
            min(restricted_trailing_chars_times), restricted_trailing_chars_times.index(min(restricted_trailing_chars_times)),
            max(restricted_trailing_chars_times), restricted_trailing_chars_times.index(max(restricted_trailing_chars_times)))
    print "  Get the distribution: {0:.3f}s ({1:.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4} chars), Max: {5:.3f}s ({6} chars)".format(
            sum(priors_times), sum(priors_times)/sum(total_times)*100,
            sum(priors_times)/len(priors_times),
            min(priors_times), priors_times.index(min(priors_times)),
            max(priors_times), priors_times.index(max(priors_times)))
    print "==================================================================="
    ch_distance = Levenshtein.distance(predict, args.sent)
    print "Final prediction: ", predict
    print "Character edit distance: {0}({1:.1f}% error rate)".format(
            ch_distance, ch_distance / float(len(args.sent)) * 100)

def draw_dist(dist, sym, rescale=True):
    dist = sorted(dist, key=lambda x:x[1])
    #print dist
    for k, (char, prob) in enumerate(dist):
      #print char, sym
      if char==sym:
        ppx = prob
        rank = k+1
    #print dist[0][0]
    return rank, dist[0][0], ppx

def pipeline(args):
    '''
    Run a pass of the pipeline based on the commandline arguments.
    '''
    path = "/Users/dudy/CSLU/bci/langmodelpy/oclm/"
    sym = fst.SymbolTable.read(path+'machines/ch_syms.bin')
    big_mrr = []
    big_edit = []
    all_data = []
    big_ppx = []
    logging.basicConfig(filename = args.log, level=logging.DEBUG)
    with io.open(args.outpath+"_.json", "w", encoding='utf8') as outfile:
        for u, sent in enumerate(open(args.sents,"r").readlines()):
            logging.info(str(u))
            lm = oclm()
            sent = sent.strip()
            lm_pred = ""
            ranks_inv = []
            ppx = []
            for j in xrange(len(sent)):
                hist = sent[:j]
                #print "hist ",hist
                eeg_saus = sausage_generator(hist, sym)
                # construct word history
                wrd_seq = lm.word_sequence_history(eeg_saus)
                # add sigma
                word_sigma = lm.add_sigma(wrd_seq)
                # take topk and form a character rep
                ltr_topk = lm.topk_choice(word_sigma)
                # union with prefix LM
                united_LM = lm.union_LMs(ltr_topk)
                # Extracts trailing characters.
                trailing_chars = lm.extract_trailing_chars(eeg_saus) 
                # Restricts the length.
                trailing_chars = lm.restrict_chars_length(trailing_chars)
                # Gets the distribution over next character.
                priors = lm.next_char_dist(trailing_chars, united_LM)
                rank, lm_guess, ltr_ppx = draw_dist(priors, sent[j], False) 
                ranks_inv.append(1/rank)
                ppx.append(ltr_ppx)
                lm_pred+=lm_guess
            user_typed_e = Levenshtein.distance(lm_pred, sent)
            ppx1 = -np.sum(ppx) / len(sent)
            #print np.sum(ppx) / len(sent)
            data = {'inv_ranks': ranks_inv, 'tst': sent, 'lm_guess': lm_pred, 'edit': user_typed_e / len(sent), 'mrr': np.mean(ranks_inv), 'ppx': ppx1, 'ppx_single': ppx }
            big_mrr.append(np.mean(ranks_inv))
            big_edit.append(user_typed_e / len(sent))
            big_ppx.append(ppx)
            logging.info("complete "+str(u))
            all_data.append(data)
        str_ = json.dumps(all_data, indent = 4, separators = (',',': '), ensure_ascii=True)
        outfile.write(unicode(str_))
    # close file and provide final numbers
    print "average MRR is: {0}".format(np.mean(big_mrr))
    print "average edit distance is {0}".format(np.mean(big_edit))
    print "perplexity is {0}".format(np.sum(big_ppx))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--sents', dest='sents', type=str,
        help='the query (underlined seperated words)')
    parser.add_argument('--out', dest='outpath', type=str,
        help='output of json file')
    parser.add_argument('--test', help='whether to test performance',
        action='store_true')
    parser.add_argument('--log', dest='log',type=str, help='logging')
    args = parser.parse_args()

    if args.test:
        pipeline_performance(args)
    else:
        pipeline(args)



