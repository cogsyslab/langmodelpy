#!/bin/bash

set -x

pushd oclm
mkdir -p machines
train=_train.txt
tst=_tst_sent.txt
trn=_trn_sent.txt
wd_sym=../wd_syms.txt
ch_sym=../ch_syms.txt
datapath=../data
out=../json_output

# special symbol labels
sigma=65535
phi=65536
rho=65537
#change working dir
echo ------------------
echo "Permanent Machines"
echo ------------------
# REFINER
gen_machine/./ref_gen.sh ${wd_sym} $sigma

# UNK2EPS
gen_machine/./unk2eps_gen.sh ${wd_sym} $rho

# LEN
gen_machine/./len_gen.sh ${wd_sym} $sigma

# WORD SIGMA
gen_machine/./wrd_sg_gen.sh ${wd_sym} $sigma

# LTR_DIST
gen_machine/./ltr_dist_gen.sh ${ch_sym} $sigma

# CH_AFTER
gen_machine/./ch_after_gen.sh ${ch_sym} $sigma

for fold in 0 1 2 3 4
do
  
  # process train files (discard words with a <5 counts and ensure compatibility with word symbol system)
  cat ${datapath}/${fold}${trn} | tr -d ' \t\r' | sed 's/#/ /g' | tr -s '[[:punct:][:space:]]' '\n' | sort | uniq -c | sort -nr > gen_machine/wrd_frq
  cat gen_machine/wrd_frq | grep -n -m1 '^\s*4\s' | egrep -o '^[^:]+' > ln_num
  num=$(cat ln_num)
  rm ln_num
  cat gen_machine/wrd_frq | head -n$(($num-1)) | awk '{print $2}' | sort > gen_machine/wrd_list
  awk 'NR==FNR{A[$1];next}$1 in A' gen_machine/wrd_list ${wd_sym} | awk '{print $1}' > wrd_intrsc

  echo ------------------
  echo "LTR2PHI"
  echo ------------------
  # LTR2PHI
  cat wrd_intrsc | python gen_machine/pfx_mklex.py > lex.txt
  gen_machine/./ltr2wrd_gen.sh lex.txt ${wd_sym} ${ch_sym} $phi

  echo ------------------
  echo "WORD_LM"
  echo ------------------
  # WORD_LM
  cat ${datapath}/${fold}${trn} | tr -d ' \t\r' | sed 's/#/ /g' > trn_sent 
  python gen_machine/wprefixCorp.py trn_sent wrd_intrsc > prf.txt
  #gen_machine/./wrd_lm_phi.sh prf.txt ${wd_sym} $phi
  gen_machine/./wrd_lm_gen.sh prf.txt ${wd_sym} 
  rm prf.txt
  rm trn_sent

  echo ------------------
  echo "LTR_LM"
  echo ------------------
  # LTR_LM
  python gen_machine/prefixCorp.py ${datapath}/${fold}${train} > ${datapath}/${fold}_corpus.txt
  gen_machine/./ch_lm_gen.sh ${datapath}/${fold}_corpus.txt ${ch_sym}

  echo ------------------
  echo "SPELLOUT"
  echo ------------------
  # SPELLOUT
  export LD_LIBRARY_PATH=/usr/local/lib/fst
  python gen_machine/spellout.py wrd_intrsc
  mv spellout.fst machines/
  rm wrd_intrsc
  rm gen_machine/wrd_list
  rm gen_machine/wrd_frq
  
  echo ------------------
  echo "Evaluate OCLM"
  echo ------------------
  python oclm.py --sents ${datapath}/${fold}${tst} --out ../json_output/OCLM${fold} --log doc${fold}
done

