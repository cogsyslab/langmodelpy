from __future__ import division
import numpy as np
from random import shuffle, randint
import math
import sys
sys.path.append('../')
from bitweight import *
import pywrapfst as fst



def eegs():
    """load EEG simulations"""
    path = "/Users/dudy/CSLU/bci/dev_langmodelpy/lm/eeg/high" 
    sample = 0
    num_sym = 0
    eeg_dict = {}
    a_sample = []
    for line in open(path).readlines():
        line = line.split()
        if line:
            a_sample.append(float(line[0]))
            num_sym+=1
        else:
            num_sym=0
            a_sample = np.array(a_sample)
            # assuming it's log likelihood and not negative ll
            transformed_dist = [BitWeight(math.e**(prob)) for prob in a_sample]
            total = sum(transformed_dist)
            normalized_dist = [(prob / total).to_real for prob in transformed_dist]
            eeg_dict[sample] = [-math.log(prob) for prob in normalized_dist]
            sample+=1
            a_sample = []
    return eeg_dict

def generate_eeg(eeg, ch):
    """generate according
    to target and non-target
    symbols a simulated
    EEG distribution"""

    # generate a tuple with all symbols (currently does not include "<")
    syms = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",\
            "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "#"]
    idx = syms.index(ch)
    del syms[idx]
    shuffle(syms)
    sample_id = randint(0, 999)
    sample = eeg[sample_id]
    dist = []
    #dist.append((ch,sample[0]))
    dist.append((ch, 0))
#    for i in xrange(len(syms)):
#        dist.append((syms[i],sample[i+1]))
#    dist = sorted(dist, key=lambda symbol: symbol[1])
    return dist

def sausage_generator_(likelihoods, hist, syms):
    """create fst machines for each sample
    and concat with history"""

    n_best = 1
    likes = likelihoods[:n_best]
    saus = fst.Fst()
    saus.set_input_symbols(syms)
    saus.set_output_symbols(syms)
    saus.add_state()
    saus.set_start(0)
    saus.add_state()
    saus.set_final(1)
    for ch, pr in likes:
        code = syms.find(ch)
        saus.add_arc(0, fst.Arc(code, code, pr, 1))
    if hist=="":
        return saus
    else:
        hist = hist.concat(saus)
        hist.arcsort(st="olabel")
        return fst.rmepsilon(hist)


def sausage_generator(sentence, sym):
    """generate non deterministic EEG history"""

    #if not eeg_path:
     #   raise ValueError('An path to EEG simulated samples is missing')

    eeg_smaples = eegs()
    saus_history = ""
    if len(sentence) == 0:
        saus_fst = fst.Fst()
        saus_fst.set_input_symbols(sym)
        saus_fst.set_output_symbols(sym)
        saus_fst.add_state()
        saus_fst.set_start(0)
        saus_fst.set_final(0)
    else:
        for i in xrange(len(sentence)):
            ch = sentence[i]
            if ch == " " or ch=="_":
                ch = "#"
            # iterationaly extract eeg likelihood for each letter
            eeg_distribution = generate_eeg(eeg_smaples, ch)
            # generate a sausage
            saus_fst = sausage_generator_(eeg_distribution, saus_history, sym)
            saus_fst.set_input_symbols(sym)
            saus_fst.set_output_symbols(sym)
            saus_history = saus_fst
    return saus_fst
