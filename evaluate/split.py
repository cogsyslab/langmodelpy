import sys
import random
from nltk.corpus import brown
import re

# shuffle sentneces
# split to 5 folds
# write 5 folds on a word level

random.seed(10)
corp = brown.sents()
X = []
for sentence in corp:
  X.append(sentence)
random.shuffle(X)

test = sys.argv[1]
train = sys.argv[2]
sent = sys.argv[3]
trn_sent = sys.argv[4]
odir = sys.argv[5]
fdata = sys.argv[6]

brownW = []
for line in open(fdata, "r").readlines():
  line = line.strip()
  line = line.split(" ")
  line = "".join(line)
  brownW.append(line)
brownW = set(brownW)

for itr in xrange(5):
  # an outer loop makes it 5 fold sets
  ftest = open(odir+str(itr)+test, "w")
  ftrain = open(odir+str(itr)+train, "w")
  fsent = open(odir+str(itr)+sent, "w")
  fsent1 = open(odir+str(itr)+trn_sent, "w")

  for fold in xrange(5):
    shard = X[fold::5]
    for item in shard:
      tst_snt = []
      trn_snt = []
      for word in item:
        word = str(word.strip()).lower()
        # rm anything that is not a-z or space
        word = re.sub(r'[^a-z\s]', "", word)
        # check if len is non-zero
        if not len(word):
          continue
        if word in brownW:
          new_word = list(word)
          new_word = " ".join(new_word)
          if fold == itr:
            ftest.write(new_word + "#")
            ftest.write("\n")
            tst_snt.append(word)
          else:
            ftrain.write(new_word)
            ftrain.write("\n")
            trn_snt.append(new_word)
        #else:
          #print word
      if tst_snt:
        fsent.write("#".join(tst_snt))
        fsent.write("\n")
      if trn_snt:
        fsent1.write(" # ".join(trn_snt))
        fsent1.write("\n")
      
  ftest.close()
  ftrain.close()
  fsent.close()
  fsent1.close()
