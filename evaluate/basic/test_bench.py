#!/usr/bin/python
from __future__ import division
import numpy as np
from scipy.stats import norm
import argparse, math
import lm_server
import sys
import pywrapfst as fst
import Levenshtein
import json
import io
import logging
# Draw the distribution.
#
# INPUTS:
#   dist, a sorted array of 2-tuple in the form of (char, probability).
#
#   rescale, a boolean which indicates whether we should rescale the probability
#   from minus logratihm space. The default value is True.
#
# OUTPUTS:
#   Print the ditribution on the screen.
#
def draw_distribution(dist, rescale=True):
    if rescale:
        dist = [[char, math.e**(-prob)] for char, prob in dist]

def draw_dist(dist, sym, rescale=True):
    dist = sorted(dist, key=lambda x:x[1]) 
    for k, (char, prob) in enumerate(dist):  
      if char==sym:
        ppx = prob 
        rank = k+1
    return rank, dist[0][0], ppx

# Parses the commandline argument.
parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group.add_argument('--copy', action='store_true',
        help='Simulate the copy task')
group.add_argument('--gen', action='store_true',
        help='Generate random word from language model')
group.add_argument('--random', action='store_true',
        help='Add random noise to the choice')
parser.add_argument('--reseed', action='store_true',
        help='reseed the random generator')
parser.add_argument('--sim', dest='sim', action='store_true',
                    help='simulates word generation task with eeg')
parser.add_argument('--lm', dest='lm', type=str,
        help='path to the language model')
parser.add_argument('--eeg', help='path to EEG synthetic data.')
parser.add_argument('--sents', dest='sents', type=str,
        help='path to test senteces')
parser.add_argument('--out', dest='outpath', type=str,
        help='path to json output')
parser.add_argument('--log', dest='fname', type=str,
        help='path to logging report')
args = parser.parse_args()
if not args.lm:
    print "the language model must be specified. see --h for help."

# Reseeds the random generator.
if args.reseed:
    np.random.seed()
else:
    np.random.seed(1437)

# Initialize the language model object
lm = lm_server.server(args.lm)

# Initializes the available charset
charset = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
           'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '<', '#']
logging.basicConfig(filename = args.fname, level=logging.DEBUG)
# Simulates the copy task. The prompt will ask user to type a word for copying.
# Then the program will show the distribution given by language model for each
# character. But the choice will always be exactly the same as the character at
# corresponding position of the original word.
if args.copy:
    big_mrr = []
    big_edit = []
    all_data = []
    big_ppx = []
    k_rank = 11
    with io.open(args.outpath+".json", "w", encoding='utf8') as outfile:
        for j, sent in enumerate(open(args.sents, "r").readlines()):
            logging.info(str(j))
            sent = sent.strip()
            correct_history = ""
            lm_pred = ""
            ranks_inv = []
            ppx = []
            recall = 0
            lm.clean_history()
            for ch in sent: # a space should be replaced with #
                # Retrieves the prior distribution over the next character and show it.
                next_ch_dist = lm.get_prior()
                rank, lm_guess, ltr_ppx = draw_dist(next_ch_dist, ch, False)
                if rank < k_rank:
                  recall+=1
                ppx.append(ltr_ppx)
                ranks_inv.append(1/rank)
                # Updates the language model with our choice.
                lm.update_symbol(ch)
                correct_history += ch
                # accumulate history of lm guesses given a correct history
                lm_pred+=lm_guess
            user_typed_e = Levenshtein.distance(lm_pred, sent)
            ppx1 =  -np.sum(ppx) / len(sent)
            data = {'inv_ranks': ranks_inv, 'tst': sent, 'lm_guess': lm_pred, 'edit': user_typed_e / len(sent), 'mrr': np.mean(ranks_inv), 'ppx': ppx1, 'ppx_single': ppx, 'recall_at_k_sent': recall/len(sent), 'recall_at_k': recall}
            big_ppx.append(ppx)
            all_data.append(data)
        str_ = json.dumps(all_data, indent = 4, separators = (',',': '), ensure_ascii=True)
        outfile.write(unicode(str_))  
    # close file and provide final numbers     
    big_mrr.append(np.mean(ranks_inv))
    big_edit.append(user_typed_e / len(sent))
    print "average MRR is: {0}".format(np.mean(big_mrr))
    print "average edit distance is {0}".format(np.mean(big_edit))
    print "perplexity is {0}".format(np.sum(big_ppx))
