#!/bin/bash
 
set -x

NGRAM=5
train_name=_train.txt
tst_sent=_tst_sent.txt
trn_sent=_trn_sent.txt
syms=$1
syms=../$syms
# make the following folders
datapath=../data
out=../json_output

# change working dir
pushd basic

for fold in 0 1 2 3 4
do 
  # Brown 6-gram character lm (C)
  farcompilestrings -symbols=$syms -keep_symbols=1 $datapath/${fold}$trn_sent | ngramcount -order=$NGRAM | ngrammake --method="kneser_ney" - > $datapath/${fold}_brown.n${NGRAM}.kn.fst
 
  # make prefix corpus
  python prefixCorp.py $datapath/${fold}$train_name > $datapath/${fold}_brown_prefix.txt

  # Brown 5-gram prefix character lm (P)
  farcompilestrings -symbols=$syms -keep_symbols=1 $datapath/${fold}_brown_prefix.txt | ngramcount -order=$NGRAM | ngrammake --method="kneser_ney" - > $datapath/${fold}_brown_prefix.n${NGRAM}.kn.fst
 
  # Use util.py to create (C<space>)*P
  python util.py $datapath/${fold}_brown.n${NGRAM}.kn.fst $datapath/${fold}_brown_prefix.n${NGRAM}.kn.fst $datapath/${fold}_brown_closure.n${NGRAM}.kn.fst

  # JASON
  echo ---------------
  echo Evaluation on $NGRAM-gram model fold ${fold}
  echo ---------------
  python test_bench.py --copy --lm $datapath/${fold}_brown.n${NGRAM}.kn.fst --sents $datapath/${fold}$tst_sent --out $out/NGRAM${fold}_ --log docNgrm${fold}
  echo ---------------
  echo Evaluation on $NGRAM-prefix model fold ${fold}
  echo ---------------
  python test_bench.py --copy --lm $datapath/${fold}_brown_closure.n${NGRAM}.kn.fst --sents $datapath/${fold}$tst_sent --out $out/PRE${fold}_ --log docPre${fold}
done
