import sys

def prefix_print(datafile):
    
    for g, line in enumerate(datafile.readlines()):
        for i in range(len(line)/2):
            print line[:i*2+1]
        print line[:-1]+" #"
if __name__ == "__main__":    
    corpus = open(sys.argv[1])
    prefix_print(corpus)
    

