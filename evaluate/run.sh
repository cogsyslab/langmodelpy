#!/bin/bash
 
set -x

test_name=_test.txt
train_name=_train.txt
tst_sent=_tst_sent.txt
trn_sent=_trn_sent.txt

# make the following folders
mkdir -p data
mkdir -p json_output
datapath=data
out=json_output
ch_sym=TBD
# word symbols and character symbols are assumed to be created and stored in main dir.
# brown.txt contains words, split.py would be slightly modified for a sentence corpus.
# the specilizer path in oclm.py should be valid
# ensure in oclm.sh that special symbols labels are correct 
# ensure bitweight and ebitweight and Levinstein lib are found

python split.py $test_name $train_name $tst_sent $trn_sent $datapath/ brown.txt
nbest=1
basic/./basic.sh $ch_sym
new_oclm/./oclm.sh $ch_sym $nbest

# EVALUATE
mkdir -p imgs

for fold in 0 1 2 3 4
do 
  mkdir -p imgs/fold${fold}
  echo "Per Sample Analysis"
  python analyze/analyze_LM.py $out/NGRAM${fold}_.json $out/PRE${fold}_.json $out/OCLM${fold}_.json imgs/fold${fold}
  echo "Per Sentence Analysis"
  Rscript analyze/mrr_sent.R OCLM PRE NGRAM /$out/ $fold
  Rscript analyze/ppx_sent.R OCLM PRE NGRAM /$out/ $fold
done
