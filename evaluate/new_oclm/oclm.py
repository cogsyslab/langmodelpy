from __future__ import division
import argparse, random
import pywrapfst as fst
import shlex, subprocess
import math
from ebitweight import BitWeight
from eeg_utils import eegs, generate_eeg
import sys
import time
import Levenshtein
sys.path.insert(0,"path to specializer")
import specializer
import logging
import io
import numpy as np
import json

#python oclm.py --sents ../data/test_0.txt --json ?? --eeg /Users/dudy/CSLU/bci/langmodelpy/lm/EEGData/EEGEvidence.txt-high --nbest 3 --specializer --machines machines/unk_test --old

def draw_dist(dist, sym, rescale=True):
    dist = sorted(dist, key=lambda x:x[1])
    #print dist
    for k, (char, prob) in enumerate(dist):
      #print char, sym
      if char==sym:
        ppx = prob
        rank = k+1
    #print dist[0][0]
    return rank, dist[0][0], ppx

def create_empty_fst(input_sym, output_sym):
    '''
    Create an empty fst (only one state being final).
    '''
    f = fst.Fst()
    f.set_input_symbols(input_sym)
    f.set_output_symbols(output_sym)
    f.add_state()
    f.set_start(0)
    f.set_final(0)
    return f

class oclm:
    def __init__(self, args):
        print 'loading predefined fsts...'
        self.spell = fst.Fst.read(args.machines + "/spellout.fst")
        self.prefix_lm = fst.Fst.read(args.machines + "/ltr_lm.fst")
        self.refiner = fst.Fst.read(args.machines + "/refiner.fst")
        self.lm = fst.Fst.read(args.machines + "/word_lm.fst")
        self.sig_mcn = fst.Fst.read(args.machines + "/sigma_machine_words.fst")
        self.ch_after_last_space_fst = fst.Fst.read(args.machines + "/ch_after_last_space.sigma.fst")
        self.ch_before_last_space_fst = fst.Fst.read(args.machines + "/ch_before_last_space.fst")
        self.length_fst = fst.Fst.read(args.machines + "/length_machine.sigma.fst")
        self.ltr_dist = fst.Fst.read(args.machines + "/ltr_dist.sigma.fst")
        self.ch_syms = fst.SymbolTable.read_text(args.machines + "/ch_syms")
        self.wd_syms = self.lm.input_symbols()
        self.SIGMA = self.wd_syms.find('<sigma>')
        self.use_specializer = args.specializer
        self.factor = args.factor
        # each element consists of the word lattice, trailing characters and the number.
        self.prefix_words = [
                [create_empty_fst(self.wd_syms, self.wd_syms),
                 create_empty_fst(self.ch_syms, self.ch_syms),
                 0]]
        # Creates the history sausage.
        self.history_fst = create_empty_fst(self.ch_syms, self.ch_syms)

        self.unk_lex = fst.Fst.read(args.machines + "/unk_ltr_lm_trans_lex.fst")
        self.no_phi_lex = fst.Fst.read(args.machines + "/iiv_lex.fst")
        self.ltr2wrd = self.__weighted_union(self.unk_lex, self.no_phi_lex, 0.5, 0.5).closure().arcsort()
        self.tr_ltr2wrd = fst.Fst.read(args.machines + "/trailing_lex.fst")

    def add_char_selfloop(self, ifst, symTbl):
        ofst = ifst.copy()
        f = fst.Fst()
        f.set_input_symbols(symTbl)
        f.set_output_symbols(symTbl)
        f.add_state()
        f.set_start(0)
        for code, ch in symTbl:
            if ch.startswith("<") or ch == "#":
                continue
            #f.add_arc(0, fst.Arc(code, code, math.log(26), 0))
            f.add_arc(0, fst.Arc(code, code, None, 0))
        f.add_state()
        space_code = symTbl.find("#")
        f.add_arc(0, fst.Arc(space_code, space_code, None, 1))
        f.set_final(1)
        ofst.concat(f)
        #ofst.rmepsilon()
        return ofst

    def normalize(self, anfst):
        """"produce a normalized fst"""
        # possibly there's a shorter way
        # that keeps all in fst land
        dist = []
        labels = []
        syms = anfst.input_symbols()
        state = anfst.start()
        flag = 0
        for arc in anfst.arcs(state):
            label = syms.find(arc.ilabel)
            pr = float(arc.weight)
            dist.append(BitWeight(pr)) # ebitweight gets -log(pr) only
            labels.append(label)
            if not flag: # init sum with fist value
                sum_value = BitWeight(pr)
                flag = 1
        # normalize distribution
        for value in dist[1:]:
            sum_value+=value # will sum in log domain (log-add)
        norm_dist = [(prob/sum_value).loge() for prob in dist]
        del anfst
        # construct a norm fst
        output = fst.Fst()
        output.set_input_symbols(syms)
        output.set_output_symbols(syms)
        output.add_state()
        output.add_state()
        for (pr, label) in zip(norm_dist,labels):
            code = syms.find(label)
            output.add_arc(0, fst.Arc(code, code, pr, 1))
        output.set_start(0)
        output.set_final(1)
        return output

    def wrd2ltr(self, fstout):
        """generate a letter fst with the topk
        words and thier corresponding weights """

        # if there are normalization methods in fst land..
        norm_fst = self.normalize(fstout)
        letter = fst.compose(norm_fst, self.spell)
        letter.push(to_final=False)
        letter.project(project_output=True)
        letter = fst.rmepsilon(letter)
        letter = fst.determinize(letter)
        for state in letter.states():
            if state==0:
                continue
            letter.set_final(state)
        return letter

    def __weighted_union(self, left, right, left_prob, right_prob):
        '''
        Union the FSTs left, right with a weight.
        '''
        # left hand side part.
        left_w = -math.log(left_prob)
        lhs = fst.Fst()
        lhs.set_input_symbols(left.input_symbols())
        lhs.set_output_symbols(left.output_symbols())
        lhs.add_state()
        lhs.set_start(0)
        lhs.add_state()
        lhs.add_arc(0, fst.Arc(0, 0, left_w, 1))
        lhs.set_final(1)
        lhs.concat(left)

        # prefix part.
        right_w = -math.log(right_prob)
        rhs = fst.Fst()
        rhs.set_input_symbols(right.input_symbols())
        rhs.set_output_symbols(right.output_symbols())
        rhs.add_state()
        rhs.set_start(0)
        rhs.add_state()
        rhs.add_arc(0, fst.Arc(0, 0, right_w, 1))
        rhs.set_final(1)
        rhs.concat(right)

        lhs.union(rhs)
        return lhs


    def combine_ch_lm(self, topk_fst, topk_wds=[]):
        '''
        Combine the spellout machine with prefix character
        language model, using the factor:
        factor * top_k + (1 - factor) * prefix_lm
        '''
        if topk_fst is None or topk_fst.num_states() == 0:
            return self.prefix_lm

        ltr_topk = self.spellout(topk_fst, topk_wds)

        return self.__weighted_union(self.prefix_lm, ltr_topk, 0.5, 0.5)

    def __get_topk_words(self, ifst):
        '''
        Returns a list of topk words with normalized weights and sorted
        by descending order.
        INPUT: ifst: an fst containing the topk choices.
        '''
        dist = []
        labels = []
        syms = ifst.input_symbols()
        state = ifst.start()
        for arc in ifst.arcs(state):
            label = syms.find(arc.ilabel)
            pr = float(arc.weight)
            dist.append(BitWeight(pr)) # ebitweight gets -log(pr) only
            labels.append(label)
        sum_value = sum(dist, BitWeight(1e6))
        norm_dist = [(prob/sum_value).real() for prob in dist]
        return sorted(zip(labels, norm_dist), key=lambda x:-x[1])


    def topk_choice(self, word_sequence, topk_wds=None):
        """extracts the topk choices of
        lm given a word history (lattice)
        input: lm.fst and sentence string
        output: topk words to complete the lattice"""

        # generate sentence fst
        fstout = fst.intersect(word_sequence, self.lm)
        fst_det = fst.determinize(fstout)
        fst_p = fst.push(fst_det, push_weights=True, to_final=True)
        fst_rm = fst.rmepsilon(fst_p)
        fst_rm = fst.determinize(fst_rm)
        short = fst.shortestpath(fst_rm, nshortest=10)
        short_det = fst.determinize(short)
        short_rm = fst.rmepsilon(short_det)
        two_state = fst.compose(short_rm, self.refiner)
        output = two_state.project(project_output=True)
        output = fst.rmepsilon(output)
        output = fst.determinize(output)
        output.minimize()
        if topk_wds is not None:  # Needs to distinguish None and []
            topk_wds.extend(self.__get_topk_words(output))
        return output

    def spellout(self, ifst, topk_wds=None):
        '''
        Spells out all the words in the ifst.
        '''
        if ifst is None:
            return None
        ofst = fst.rmepsilon(ifst)
        ofst = fst.determinize(ofst)
        ofst.minimize()
        if topk_wds is not None:
            topk_wds.extend(self.__get_topk_words(ofst))
        return self.wrd2ltr(ofst)

    def add_sigma(self, fst_machine):
        # soon to be replaced
        # with a python operation
        #fst_machine = fst.compose(self.unfinal, fst_machine)
        fst_machine.concat(self.sig_mcn)
        fst_machine.rmepsilon()
        fst_machine.arcsort(st="olabel")
        if self.use_specializer:
            output = specializer.sigma(fst_machine, self.SIGMA, "always").get()
            return output
        else:
            fname = "wrd_seq.fst"
            fst_machine.write(fname)
            command_line = "./specialize.sh "+fname
            args = shlex.split(command_line)
            p = subprocess.Popen(args, stdout=subprocess.PIPE)
            output = p.communicate()[0]
            fname = "new_"+fname
            return fst.Fst.read(fname)

    def word_sequence_history(self, eeg_saus):
        """generate a probable word
        sequence given the EEG samples
        by intersecting it with word
        language model"""

        word_seq = fst.compose(eeg_saus, self.ltr2wrd)
        fst.push(word_seq, push_weights=True, to_final=True)
        word_seq.project(project_output=True)
        word_seq.rmepsilon()
        return word_seq
        # the motivation to have a function is to
        # free the memory from ltr2wrd once the
        # intersection is done

    def separate_sausage(self, sausage, helper_fst):
        '''
        Separates history sausage based on the last space. The direction
        (before/after) depends on the helper fst passed in.
        '''
        chars = fst.compose(sausage, helper_fst)
        chars.project(True)
        chars.rmepsilon()
        chars = fst.determinize(chars)
        chars.minimize().topsort()
        return chars

    def restrict_chars_length(self, trailing_chars):
        '''
        Restrict the length of trailing characters to avoid complex computation.
        The exact number of length depending on the implementation of fst.
        '''
        restricted_length = fst.compose(trailing_chars, self.length_fst)
        restricted_length.project(project_output=True)
        restricted_length.rmepsilon()
        restricted_length = fst.determinize(restricted_length)
        restricted_length.minimize()
        return restricted_length

    def concat_alphabet(self, ifst):
        '''
        Concatenate all characters in the alphabet to the end of a given fst.
        '''
        ofst = ifst.copy()
        sigma = fst.Fst()
        sigma.set_input_symbols(ofst.input_symbols())
        sigma.set_output_symbols(ofst.output_symbols())
        sigma.add_state()
        sigma.set_start(0)
        for code, ch in ofst.input_symbols():
            if ch.startswith("<"):  # Don't include special arcs in sigma machine.
                continue
            sigma.add_arc(0, fst.Arc(code, code, None, 1))
        sigma.add_state()
        sigma.set_final(1)
        ofst.concat(sigma)
        ofst.rmepsilon()
        return ofst

    def next_char_dist(self, history, char_lm):
        '''
        Get the distribution of next character.
        '''
        history = self.concat_alphabet(history)
        history.arcsort(st="olabel")
        output = fst.intersect(history, char_lm)
        output.rmepsilon()
        output = fst.determinize(output)
        output.minimize()

        # reads an fst to combine the weights of the next character.
        last_ltr = fst.compose(output, self.ltr_dist)
        last_ltr.project(True)
        last_ltr.push(to_final=True)
        last_ltr.rmepsilon()
        last_ltr = fst.determinize(last_ltr)
        last_ltr.minimize()

        # Extracts priors. Although it's a two-state machine, we have the
        # generic traverse procedure here just in case.
        prev_stateid = curr_stateid = None
        for state in last_ltr.states():
            if not curr_stateid is None:
                prev_stateid = curr_stateid
            curr_stateid = state
        priors = []
        syms = last_ltr.input_symbols()
        for arc in last_ltr.arcs(prev_stateid):
            ch = syms.find(arc.ilabel)
            w = float(arc.weight)
            if len(ch) == 1:
                priors.append((ch, w))

        # Sorts the prior by the probability and normalize it.
        priors = sorted(priors, key=lambda prior: prior[1])
        priors_vals = [BitWeight(prob) for _,prob in priors]
        total = sum(priors_vals, BitWeight(1e6))
        norm_priors = [(prob / total).loge() for prob in priors_vals]
        return zip([ch for ch,_ in priors], norm_priors)

    def append_eeg_evidence(self, ch_dist):
        new_ch = fst.Fst()
        new_ch.set_input_symbols(self.ch_syms)
        new_ch.set_output_symbols(self.ch_syms)
        new_ch.add_state()
        new_ch.set_start(0)
        new_ch.add_state()
        new_ch.set_final(1)
        for ch, pr in ch_dist:
            code = self.ch_syms.find(ch)
            new_ch.add_arc(0, fst.Arc(code, code, pr, 1))
        new_ch.arcsort(st="olabel")
        self.history_fst.concat(new_ch).rmepsilon()


    def update(self, ch_dist):
        '''
        Update the history with the new likelihood array in the correct scale
        (nagative log space) to the history.
        '''
        new_ch = fst.Fst()
        new_ch.set_input_symbols(self.ch_syms)
        new_ch.set_output_symbols(self.ch_syms)
        new_ch.add_state()
        new_ch.set_start(0)
        new_ch.add_state()
        new_ch.set_final(1)
        space_code = -1
        space_pr = 0.
        for ch, pr in ch_dist:
            code = self.ch_syms.find(ch)
            if ch == '#':  # Adds space after we finish updating trailing chars.
                space_code = code
                space_pr = pr
                continue
            new_ch.add_arc(0, fst.Arc(code, code, pr, 1))
        new_ch.arcsort(st="olabel")

        # Adds the trailing characters to existing binned history.
        for words_bin in self.prefix_words:
            if words_bin[2] >= 10:  # We discard the whole trail in this case (TODO)
                continue
            # Unless we are testing a straight line machine, this normally
            # doesn't happen in practice.
            if new_ch.num_arcs(0) == 0:
                continue
            words_bin[1].concat(new_ch).rmepsilon()
            words_bin[2] += 1

        # Continues updating the history and adds back the space if necessary.
        if space_code >= 0:
            new_ch.add_arc(0, fst.Arc(space_code, space_code, space_pr, 1))
        self.history_fst.concat(new_ch).rmepsilon()

        # Respectively update the binned history
        if space_code >= 0:  # If there is a space
            # Finishes the prefix words in current position
            word_lattice = fst.compose(self.history_fst, self.ltr2wrd)
            word_lattice.project(project_output=True).rmepsilon()
            word_lattice = fst.determinize(word_lattice)
            word_lattice.minimize()
            if word_lattice.num_states() == 0:
                word_lattice = create_empty_fst(self.wd_syms, self.wd_syms)
            trailing_chars = create_empty_fst(self.ch_syms, self.ch_syms)
            self.prefix_words.append([word_lattice, trailing_chars, 0])

    def predict(self, top_k_wds=[]):
        top_k = None
        trailing_chars = None
        for wd_lattice, tr_chars, nchars in self.prefix_words:
            wd_lattice_cp = wd_lattice.copy()
            if nchars >= 10: continue  # (TODO)
            if tr_chars.num_states() == 1:
                # Add sigma if we want to start a new word
                word_seq = self.add_sigma(wd_lattice_cp)
            else:
                tr_sigma = self.add_char_selfloop(tr_chars, self.ch_syms)
                tr_sigma.arcsort(st="olabel")
                tr_wds = fst.compose(tr_sigma, self.ltr2wrd)
                tr_wds.project(project_output=True).rmepsilon()
                # If we can't even find candidates for current trailing chars,
                # skip this bin.
                if tr_wds.num_states() == 0:
                    continue
                '''
                word_seq = fst.push(wd_lattice_cp.concat(tr_wds).rmepsilon(),
                                    push_weights=True, to_final=True)
                '''
                word_seq = wd_lattice_cp.concat(tr_wds).rmepsilon()
            if not top_k:
                top_k = self.topk_choice(word_seq)
            else:
                top_k.union(self.topk_choice(word_seq))
            if not trailing_chars:
                trailing_chars = tr_chars
            else:
                trailing_chars.union(tr_chars)
        #print "TOP K: \n",top_k_wds
        #print "Trailing Char: \n", trailing_chars
        united_LM = self.combine_ch_lm(top_k, top_k_wds)
        priors = self.next_char_dist(trailing_chars, united_LM)
        return priors


def pipeline_performance(args):
    '''
    Use the commandline arguments to measure the performance of our pipeline.
    (Obsolete for now)
    '''
    eeg_samples = eegs(args.eeg)  # Reads EEG samples
    sym = fst.SymbolTable.read_text(args.machines + '/ch_syms')
    lm = oclm(args)

    # Timer
    separate_times = []
    word_lattice_times = []
    current_words_times = []
    topk_times = []
    united_LM_times = []
    priors_times = []
    total_times = []

    # Main Process
    for i, ch in enumerate(args.sent):
        #print args.sent[:i] + "_"*(len(args.sent)-i)
        if ch == " " or ch == "_":
            ch = "#"
        eeg_dist = generate_eeg(eeg_samples, ch)
        nbest_eeg_dist = eeg_dist[:args.nbest]
        # Normalizes the EEG evidence.
        tmp_dst = [BitWeight(pr) for _,pr in nbest_eeg_dist]
        total = sum(tmp_dst, BitWeight(1e6))
        norm_tmp_dst = [(pr / total).loge() for pr in tmp_dst]
        nbest_eeg_dist = zip([ch for ch,_ in nbest_eeg_dist], norm_tmp_dst)
        #print "NBEST EEG EVIDENCE"
        #print nbest_eeg_dist
        start_time = time.time()
        if args.old:
            history_chars = lm.separate_sausage(lm.history_fst, lm.ch_before_last_space_fst)
            trailing_chars = lm.separate_sausage(lm.history_fst, lm.ch_after_last_space_fst)
            separate_time = time.time()
            separate_times.append(separate_time - start_time)

            word_lattice = fst.compose(history_chars, lm.ltr2wrd)
            #word_lattice = fst.push(word_lattice, push_weights=True, to_final=True)
            word_lattice.project(project_output=True).rmepsilon()
            word_lattice = fst.determinize(word_lattice)
            word_lattice.minimize().topsort()
            #print "WORD LATTICE BRFORE WORD LM"
            #print word_lattice
            if word_lattice.num_states() == 0:
                word_lattice = create_empty_fst(lm.wd_syms, lm.wd_syms)
            word_lattice_time = time.time()
            word_lattice_times.append(word_lattice_time - separate_time)

            trailing_chars_sigma = lm.add_char_selfloop(trailing_chars, sym)
            trailing_chars_sigma.arcsort(st="olabel")
            current_words = fst.compose(trailing_chars_sigma, lm.no_phi_lex)
            current_words.project(project_output=True).rmepsilon()
            current_words = fst.determinize(current_words)
            current_words.minimize().topsort
            current_words_time = time.time()
            current_words_times.append(current_words_time - word_lattice_time)

            word_seq = word_lattice.copy().concat(current_words).rmepsilon()
            topk_wds = []
            topk = lm.topk_choice(word_seq)
            topk_time = time.time()
            topk_times.append(topk_time - current_words_time)

            united_LM = lm.combine_ch_lm(topk, topk_wds)
            united_LM_time = time.time()
            united_LM_times.append(united_LM_time - topk_time)

            #print "TOPK WORDS:"
            #for wd, prob in topk_wds:
            #    print wd, '\t', prob
            #print
            trailing_chars = lm.restrict_chars_length(trailing_chars)
            #print "Trailling Chars"
            #print trailing_chars

            priors = lm.next_char_dist(trailing_chars, united_LM)
            lm.append_eeg_evidence(nbest_eeg_dist)
            priors_time = time.time()
            priors_times.append(priors_time - united_LM_time)

            execution_time = time.time()
            total_times.append(execution_time - start_time)


            # Outputs the distribution
            print "==================================================================="
            for ch, p in priors:
                print "{0}\t{1:.3f}".format(ch, p)
            print "==================================================================="
            print "Separate Sausage:    {0:.3f}s".format(separate_time - start_time)
            print "Create Word Lattice: {0:.3f}s".format(word_lattice_time - separate_time)
            print "Find Current Word:   {0:.3f}s".format(current_words_time - word_lattice_time)
            print "Find TopK Words:     {0:.3f}s".format(topk_time - current_words_time)
            print "Construct CH LM:     {0:.3f}s".format(united_LM_time - topk_time)
            print "Find Priors:         {0:.3f}s".format(priors_time - united_LM_time)
            print "Total Time:          {0:.3f}s".format(execution_time - start_time)
            raw_input("")

    # Outputs the performance stats.
    print "==================================================================="
    print "Total time:            {0:.3f}s, Avg: {1:.3f}s, Min: {2:.3f}s ({3} chars), Max: {4:.3f}s ({5} chars)".format(
            sum(total_times), sum(total_times)/float(len(total_times)), min(total_times),
            total_times.index(min(total_times)), max(total_times), total_times.index(max(total_times)))
    print "  Separate Sausage:    {0:.3f}s ({1:2.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4}th char), Max: {5:.3f}s ({6}th char)".format(
            sum(separate_times), sum(separate_times)/sum(total_times)*100,
            sum(separate_times)/len(separate_times),
            min(separate_times), separate_times.index(min(separate_times)),
            max(separate_times), separate_times.index(max(separate_times)))
    print "  Create Word Lattice: {0:.3f}s ({1:2.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4}th char), Max: {5:.3f}s ({6}th char)".format(
            sum(word_lattice_times), sum(word_lattice_times)/sum(total_times)*100,
            sum(word_lattice_times)/len(word_lattice_times),
            min(word_lattice_times), word_lattice_times.index(min(word_lattice_times)),
            max(word_lattice_times), word_lattice_times.index(max(word_lattice_times)))
    print "  Find Current Word:   {0:.3f}s ({1:2.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4}th char), Max: {5:.3f}s ({6}th char)".format(
            sum(current_words_times), sum(current_words_times)/sum(total_times)*100,
            sum(current_words_times)/len(current_words_times),
            min(current_words_times), current_words_times.index(min(current_words_times)),
            max(current_words_times), current_words_times.index(max(current_words_times)))
    print "  Find TopK Words:     {0:.3f}s ({1:2.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4}th char), Max: {5:.3f}s ({6}th char)".format(
            sum(topk_times), sum(topk_times)/sum(total_times)*100,
            sum(topk_times)/len(topk_times),
            min(topk_times), topk_times.index(min(topk_times)),
            max(topk_times), topk_times.index(max(topk_times)))
    print "  Construct CH LM:     {0:.3f}s ({1:2.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4}th char), Max: {5:.3f}s ({6}th char)".format(
            sum(united_LM_times), sum(united_LM_times)/sum(total_times)*100,
            sum(united_LM_times)/len(united_LM_times),
            min(united_LM_times), united_LM_times.index(min(united_LM_times)),
            max(united_LM_times), united_LM_times.index(max(united_LM_times)))
    print "  Find Priors:         {0:.3f}s ({1:2.1f}%), Avg: {2:.3f}s, Min: {3:.3f}s ({4}th char), Max: {5:.3f}s ({6}th char)".format(
            sum(priors_times), sum(priors_times)/sum(total_times)*100,
            sum(priors_times)/len(priors_times),
            min(priors_times), priors_times.index(min(priors_times)),
            max(priors_times), priors_times.index(max(priors_times)))
    print "==================================================================="

def pipeline(args):
    '''
    Run a pass of the pipeline based on the commandline arguments.
    '''
    eeg_samples = eegs(args.eeg)  # Reads EEG samples
    sym = fst.SymbolTable.read_text(args.machines + '/ch_syms')
    big_mrr = []
    big_edit = []
    all_data = []
    big_ppx = []
    logging.basicConfig(filename = args.log, level=logging.DEBUG)
    with io.open(args.outpath+"_.json", "w", encoding='utf8') as outfile:
        for j, sent in enumerate(open(args.sents,"r").readlines()):
            logging.info(str(j))
            lm = oclm(args)
            sent = sent.strip()
            lm_pred = ""
            ranks_inv = []
            ppx = []
            for i, ch in enumerate(sent):
                #print sent[:i] + "_"*(len(sent)-i)
                if ch == " " or ch == "_":
                    ch = "#"
                eeg_dist = generate_eeg(eeg_samples, ch)
                nbest_eeg_dist = eeg_dist[:args.nbest]
                # Normalizes the EEG evidence.
                '''
                tmp_dst = [BitWeight(pr) for _,pr in nbest_eeg_dist]
                total = sum(tmp_dst, BitWeight(1e6))
                norm_tmp_dst = [(pr / total).loge() for pr in tmp_dst]
                nbest_eeg_dist = zip([ch for ch,_ in nbest_eeg_dist], norm_tmp_dst)
                '''
                #print "NBEST EEG EVIDENCE"
                #print nbest_eeg_dist
                start_time = time.time()
                if args.old:
                    history_chars = lm.separate_sausage(lm.history_fst, lm.ch_before_last_space_fst)
                    trailing_chars = lm.separate_sausage(lm.history_fst, lm.ch_after_last_space_fst)

                    word_lattice = fst.compose(history_chars, lm.ltr2wrd)
                    #word_lattice = fst.push(word_lattice, push_weights=True, to_final=True)
                    word_lattice.project(project_output=True).rmepsilon()
                    word_lattice = fst.determinize(word_lattice)
                    word_lattice.minimize().topsort()
        #            print "WORD LATTICE BRFORE WORD LM"
        #            print word_lattice
                    if word_lattice.num_states() == 0:
                        word_lattice = create_empty_fst(lm.wd_syms, lm.wd_syms)

                    trailing_chars_sigma = lm.add_char_selfloop(trailing_chars, sym)
                    trailing_chars_sigma.arcsort(st="olabel")
                    current_words = fst.compose(trailing_chars_sigma, lm.tr_ltr2wrd)
                    current_words.project(project_output=True).rmepsilon()
                    current_words = fst.determinize(current_words)
                    current_words.minimize().topsort()

                    #print current_words
                    word_seq = word_lattice.copy().concat(current_words).rmepsilon()
                    #print word_seq
                    topk_wds = []
                    topk = lm.topk_choice(word_seq)
                    united_LM = lm.combine_ch_lm(topk, topk_wds)
        #            print "TOPK WORDS:"
        #            for wd, prob in topk_wds:
        #                print wd, '\t', prob
        #            print
                    #trailing_chars = lm.restrict_chars_length(trailing_chars)
        #            print "Trailling Chars"
        #            print trailing_chars
                    priors = lm.next_char_dist(trailing_chars, united_LM)
                    lm.append_eeg_evidence(nbest_eeg_dist)
                else:
                    priors = lm.predict()
                    lm.update(nbest_eeg_dist)
                rank, lm_guess, ltr_ppx = draw_dist(priors, sent[i], False) 
                ranks_inv.append(1/rank)
                ppx.append(ltr_ppx)
                lm_pred+=lm_guess
            user_typed_e = Levenshtein.distance(lm_pred, sent)
            ppx1 = -np.sum(ppx) / len(sent)
            #print np.sum(ppx) / len(sent)
            data = {'inv_ranks': ranks_inv, 'tst': sent, 'lm_guess': lm_pred, 'edit': user_typed_e / len(sent), 'mrr': np.mean(ranks_inv), 'ppx': ppx1, 'ppx_single': ppx }
            big_mrr.append(np.mean(ranks_inv))
            big_edit.append(user_typed_e / len(sent))
            big_ppx.append(ppx)
            logging.info("complete "+str(j))
            all_data.append(data)
        str_ = json.dumps(all_data, indent = 4, separators = (',',': '), ensure_ascii=True)
        outfile.write(unicode(str_))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--sents', dest='sents', type=str,
        help='the query (underlined seperated words)')
    parser.add_argument('--out', dest='outpath', type=str,
        help='output of json file')
    parser.add_argument('--eeg', dest='eeg', type=str,
        help='the eeg samples file')
    parser.add_argument('--test', help='whether to test performance',
        action='store_true')
    parser.add_argument('-n', '--nbest', type=int, default=3,
        help='Chooses nbest eeg samples for simulation')
    parser.add_argument('--specializer', action='store_true')
    parser.add_argument('--old', action='store_true')
    parser.add_argument('--machines', help='folders containing all predefined fsts.')
    parser.add_argument('--factor', type=float, default=0.5, help='how much factor we put on topk words.')
    parser.add_argument('--log', dest='log',type=str, help='logging')
    
    args = parser.parse_args()

    random.seed(1437)
    if args.test:
        pipeline_performance(args)
    else:
        pipeline(args)
