import sys
import json
import matplotlib.pyplot as plt
import numpy as np
import six
import matplotlib
from matplotlib import rcParams
matplotlib.rc('font', family='Tahoma')
rcParams.update({'figure.autolayout': True})

def determine_len(sen):
    sp_idx = [i for i in xrange(len(sen)) if sen[i]=="#"]
    if sp_idx == [] or len(sp_idx) < 5 :
        return len(sen)
    else:
        th = 40
        tmp = sp_idx[0]
        for idx in sp_idx[1:]:
            if np.abs(tmp-th) > np.abs(idx-th):
                tmp = idx
        return tmp
    
# settings
def RR(sent, s1, s2, s3, r1, r2, r3, ed1, ed2, ed3, name, font_size=10, edge_color='w', header_color='grey',\
              header_columns=0, col_width=3.0, row_height=2, lwd=0.2, row_colors=['#f1f1f2', 'w'], colors=['magenta', 'orange', 'green']):
    ll = determine_len(sent)
    #ll = len(sent)
    op=0.6
    fig, axs = plt.subplots(2,1)
    clust_data = [s1[:ll], s2[:ll], s3[:ll]]
    ind = np.arange(len(sent[:ll]))
    lb1 = '{:>4} {:0.2f}'.format("MRR:" ,np.mean(r1[:ll])) 
    lb2 = '{:>4} {:0.2f}'.format("MRR:", np.mean(r2[:ll])) 
    lb3 = '{:>4} {:0.2f}'.format("MRR:", np.mean(r3[:ll])) 
    plt1 = axs[0].plot(ind, np.array(r1[:ll]), 'r-o', color='magenta', label=lb1, linewidth=lwd, alpha=op)
    plt2 = axs[0].plot(ind, np.array(r2[:ll])+0.01, 'r-o', color='orange', label=lb2, linewidth=lwd, alpha=op)
    plt3 = axs[0].plot(ind, np.array(r3[:ll])+0.02, 'r-o', color='green', label=lb3, linewidth=lwd, alpha=op)
    for i in xrange(len(sent[:ll])):
        axs[0].plot((i+0.5,i+0.5),(0,1),'w--', color='#f1f1f2')
    axs[0].set_xlim([-0.5, len(sent[:ll])-0.5])
    axs[0].set_ylim([0,1])
    axs[0].set_ylabel('RR')
    axs[0].get_xaxis().set_visible(False)
    box = axs[0].get_position()
    axs[0].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    axs[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.9),fancybox=True, ncol=3)
    collabel = tuple(list(sent))
    rowlabels = ['ngrmLM', 'preLM', 'OCLM']
    a = np.chararray((3,len(sent[:ll])))
    a[0] = list(s1[:ll])
    a[1] = list(s2[:ll])
    a[2] = list(s3[:ll])
    axs[1].axis('tight')
    axs[1].axis('off')
    table = axs[1].table(cellText=a, colLabels=collabel, rowColours=colors ,rowLabels=rowlabels, loc='center', cellLoc='center')
    table.auto_set_font_size(False)
    table.set_fontsize(font_size)
    i=0
    for k, cell in six.iteritems(table._cells):
        cell.set_edgecolor(edge_color)
        if k[0] == 0 or k[1] < header_columns:
            cell.set_text_props(weight='bold', color='w', wrap=True)
            cell.set_facecolor(header_color)
            if k[1] < header_columns:
              cell.set_facecolor(colors[i])
              i+=1
        else:
            cell.set_facecolor(row_colors[k[0]%len(row_colors) ])

    fig.subplots_adjust(hspace=0)
    plt.savefig(name+'.pdf')
    #plt.show()

