#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

# test if there is at least one argument: if not, return an error
if (length(args)!=5) {
  stop("Not a correct number of paths (input file).n", call.=FALSE)
} else {
  # default output file
  file1=args[1]
  file2=args[2]
  file3=args[3]
  jsonpath=args[4]
  g=args[5]
}

library("ggplot2")
library("rjson")
library("stringr")
wd<-getwd()

df = data.frame()

json_file<-paste(wd,jsonpath, sep="")
json_file<-paste(json_file, file1, sep="")
json_file<-paste(json_file,g, sep = "")
json_file<-paste(json_file,"_.json", sep="")
json_data<-fromJSON(file=json_file)
cls = file1
for(n in json_data){
  ln<-str_length(n$tst)
  ppx = -n$ppx
  newrow = data.frame(ppx)
  df = rbind(df, newrow)
}

df1 = data.frame()

json_file<-paste(wd,jsonpath, sep="")
json_file<-paste(json_file, file2, sep="")
json_file<-paste(json_file,g, sep = "")
json_file<-paste(json_file,"_.json", sep="")
json_data<-fromJSON(file=json_file)
cls = file2
for(n in json_data){
  ln<-str_length(n$tst)
  ppx = -n$ppx
  newrow = data.frame(ppx)
  df1 = rbind(df1, newrow)
}

df2 = data.frame()

json_file<-paste(wd,jsonpath, sep="")
json_file<-paste(json_file, file3, sep="")
json_file<-paste(json_file,g, sep = "")
json_file<-paste(json_file,"_.json", sep="")
json_data<-fromJSON(file=json_file)
cls = file3
for(n in json_data){
  ln<-str_length(n$tst)
  ppx = -n$ppx
  newrow = data.frame(ppx)
  df2 = rbind(df2, newrow)
}

df3 = data.frame(cls1=df, cls2=df1, cls3=df2)
tit<-"LMs' Sentence PPX"
fname<-paste(wd,"/imgs/fold",g , sep="")
fname<-paste(fname,"/ppx_boxplot", sep="")
pdf(file=paste(fname, ".pdf", sep=""))
boxplot(df3, names=c(file1,file2,file3))
mtext(2, text="sentence PPX", line=2.5)
dev.off() 
