from __future__ import division
import sys
from preprocess import sent_tuples
import pandas as pd
import matplotlib.pyplot as plt
import pdb
import math 
import numpy as np
import seaborn as sns
from matplotlib import ticker


#matplotlib.rcParams.update({'font.size': 152})
sns.set(style="darkgrid")


fname1 = sys.argv[1]
fname2 = sys.argv[2]

s_pairs1 = sent_tuples(fname1)
s_pairs2 = sent_tuples(fname2)
ft=20

if 1:
  fig = plt.figure(1, figsize=(10,10))
  mrr = []
  w_len = []
  mrr2 = []
  for (tgt,ranks, ratk ), (tgt1, ranks1, ratk1) in zip(s_pairs1, s_pairs2):
    ln = len(tgt)
    w_len.append(ln)
    mrr.append(sum(ranks)/ln)
    mrr2.append(sum(ranks1)/ln)
  df = pd.DataFrame({'length': w_len, "LM's sentence MRR": mrr, ' ': mrr2 })

#  ax1 = fig.add_subplot(3,1,1)
#  #bo = sns.boxplot(x='length', y="LM's sentence MRR", data=df, ax=ax1)
#  bo = df.boxplot(column="LM's sentence MRR", by='length', ax=ax1)
  medians = df.groupby(['length'])["LM's sentence MRR"].median().values
#  ax1.set_xlabel("")
#  ax1.set_ylabel("OCLM", fontsize=ft)
  st = []
  for i in range(1,301):
    if i%50==0:
      st.append(str(i))
    else:
      st.append('')
  xticks = ticker.IndexFormatter(st)
#  ax1.xaxis.set_major_formatter(xticks)
#  ax1.set_title("LM's sentence MRR", fontsize=ft)
#  ax1.tick_params(labelsize='large')
#  fig.suptitle("")
#
#  ax2 = fig.add_subplot(3,1,2)
#  #bo = sns.boxplot(x='length', y=' ', data=df, ax=ax2)
#  bo = df.boxplot(column=" ", by='length', ax=ax2)
  medians2 = df.groupby(['length'])[' '].median().values
  medi = medians-medians2
#  medi = medi[medi>0] # 292 times of 300 OCLM is bigger than PRE
#  print len(medi)
#
#  ax2.set_xlabel("")
#  ax2.set_ylabel("PRE", fontsize=ft)
#  ax2.xaxis.set_major_formatter(xticks)
#  ax2.tick_params(labelsize='large')
#  fig.suptitle("")
#
#  ax3 = fig.add_subplot(3,1,3)
#  sns.countplot(x="length", data=df, ax=ax3, facecolor='grey')
#  plt.yscale('log')
#  ax3.xaxis.set_major_formatter(xticks)
#  ax3.tick_params(labelsize='large')
#  ax3.set_xlabel("length", size=ft)
#  ax3.set_ylabel("count", size=ft)
#  plt.savefig("sent_mrr.pdf", format='pdf', dpi=100)

#  df3 = pd.DataFrame({"sentence length": range(1,301), "OCLM-PRE": medi})
  ax1 = fig.add_subplot(1,1,1)
  ax1.plot(range(1,301), medi)
#  plt.yscale('log')
#  ax1.xaxis.set_major_formatter(xticks)
  ax1.tick_params(labelsize='large')
  ax1.set_xlabel("sentence length", size=ft)
  ax1.set_ylabel("OCLM-PRE (medians of sentence MRR)", size=ft)

  plt.savefig("sent_mrr_med.pdf", format='pdf', dpi=100)

else:
  fig = plt.figure(2, figsize=(10,10))
  rr = []
  rr2 = []
  pos = []
  for (tgt,ranks, ratk), (tgt1, ranks1, ratk1) in zip(s_pairs1, s_pairs2):
    for i , (t, p, t1, p1) in  enumerate(zip(tgt, ranks, tgt1, ranks1)):
      pos.append(i+1)
      rr.append(p) 
      rr2.append(p1) 
  df = pd.DataFrame({'position': pos, 'position RR': rr, ' ': rr2})

  ax1 = fig.add_subplot(3,1,1)
  bo = df.boxplot(column='position RR', by='position', ax=ax1)
  medians = df.groupby(['position'])['position RR'].median().values
  ax1.set_xlabel("")
  ax1.set_ylabel("OCLM", fontsize=ft)
  st = []
  for i in range(1,301):
    if i%50==0:
      st.append(str(i))
    else:
      st.append('')
  xticks = ticker.IndexFormatter(st)
  ax1.xaxis.set_major_formatter(xticks)
  ax1.set_title("LM's position RR", fontsize=ft)
  ax1.tick_params(labelsize='large')
  plt.suptitle("")

  ax2 = fig.add_subplot(3,1,2)
  bo = df.boxplot(column=' ', by='position', ax=ax2)
  medians2 = df.groupby(['position'])[' '].median().values
  medi = medians-medians2
  medi = medi[medi>0] # 23 times of 300 bigger than PRE, 0 times of 300 PRE is bigger than OCLM
  print len(medi)
  #print medians
  #print medians2
  ax2.set_xlabel("")
  ax2.set_ylabel("PRE", fontsize=ft)
  ax2.xaxis.set_major_formatter(xticks)
  ax2.tick_params(labelsize='large')
  plt.suptitle("")

  ax3 = fig.add_subplot(3,1,3)
  sns.countplot(x="position", data=df, ax=ax3, facecolor="grey")
  ax3.xaxis.set_major_formatter(xticks)
  ax3.tick_params(labelsize='large')
  ax3.set_xlabel("position", size=ft)
  ax3.set_ylabel("count", size=ft)
  plt.yscale('log')
  plt.savefig("sent_pos.pdf", format='pdf', dpi=100)
