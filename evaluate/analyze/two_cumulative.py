from __future__ import division
from scipy import stats
import sys
import json
import matplotlib.pyplot as plt
import numpy as np
import six
import matplotlib
from matplotlib import rcParams
from visualize import determine_len
matplotlib.rc('font', family='Tahoma')
rcParams.update({'figure.autolayout': True})
from matplotlib import colors as mcolors

color_list = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)

# settings
def cumulative2(sent, s1, s2, s3, r1, r2, r3, p1, p2, p3, ed1, ed2, ed3, name, font_size=10, edge_color='w', header_color='grey',\
              header_columns=0, col_width=3.0, row_height=2, lwd=0.7, row_colors=['#f1f1f2', 'w'], colors=['magenta', color_list['green'], 'dodgerblue']):
    ll = determine_len(sent)
    #ll = len(sent)
    op=0.6
    fig, axs = plt.subplots(3,1)
    clust_data = [s1[:ll], s2[:ll], s3[:ll]]
    ind = np.arange(len(sent[:ll]))

    r1 = np.array(r1[:ll])
    r2 = np.array(r2[:ll])
    r3 = np.array(r3[:ll])
    cum1 = [np.sum(r1[:i])/np.sum(r1) for i in xrange(len(r1))]
    cum2 = [np.sum(r2[:i])/np.sum(r2) for i in xrange(len(r2))]
    cum3 = [np.sum(r3[:i])/np.sum(r3) for i in xrange(len(r3))]
    x = range(len(r1))
    sl10, _, _, _, _ = stats.linregress(x[:int(len(x)/2)],cum1[:int(len(x)/2)])
    sl11, _, _, _, _ = stats.linregress(x[int(len(x)/2):],cum1[int(len(x)/2):]) 
    sl1_all, _, _, _, _ = stats.linregress(x,cum1)
    sl20, _, _, _, _ = stats.linregress(x[:int(len(x)/2)],cum2[:int(len(x)/2)])
    sl21, _, _, _, _ = stats.linregress(x[int(len(x)/2):],cum2[int(len(x)/2):])
    sl2_all, _, _, _, _ = stats.linregress(x,cum2)
    sl30, _, _, _, _ = stats.linregress(x[:int(len(x)/2)],cum3[:int(len(x)/2)])
    sl31, _, _, _, _ = stats.linregress(x[int(len(x)/2):],cum3[int(len(x)/2):])
    sl3_all, _, _, _, _ = stats.linregress(x,cum3)
    sl10 = sl10/sl1_all*100
    sl20 = sl20/sl2_all*100
    sl30 = sl30/sl3_all*100
    sl11 = sl11/sl1_all*100
    sl21 = sl21/sl2_all*100
    sl31 = sl31/sl3_all*100
    #lb1 = '{}: {:0.1f} \n{}: {:0.1f}'.format(r'$1^{st}$', sl10/sl1_all*100, r'$2^{nd}$', sl11/sl1_all*100) 
    #lb2 = '{}: {:0.1f} \n{}: {:0.1f}'.format(r'$1^{st}$', sl20/sl2_all*100, r'$2^{nd}$', sl21/sl2_all*100) 
    #lb3 = '{}: {:0.1f} \n{}: {:0.1f}'.format(r'$1^{st}$', sl30/sl3_all*100, r'$2^{nd}$', sl31/sl3_all*100) 
    slopes = [sl10, sl20, sl30, sl11, sl21, sl31]
    sgn = []
    for sl in slopes:
        if sl < 100:
            sgn.append("-")
        else:
            sgn.append("+")

    lb1 = '{}: {} \n{}: {}'.format(r'$1^{st}$', sgn[0], r'$2^{nd}$', sgn[1]) 
    lb2 = '{}: {} \n{}: {}'.format(r'$1^{st}$', sgn[2], r'$2^{nd}$', sgn[3]) 
    lb3 = '{}: {} \n{}: {}'.format(r'$1^{st}$', sgn[4], r'$2^{nd}$', sgn[5]) 
    plt1 = axs[0].plot(ind, cum1, 'r-o', color='magenta', label=lb1, linewidth=lwd, alpha=op)
    plt2 = axs[0].plot(ind, cum2, 'r-o', color=color_list['green'], label=lb2, linewidth=lwd, alpha=op)
    plt3 = axs[0].plot(ind, cum3, 'r-o', color='dodgerblue', label=lb3, linewidth=lwd, alpha=op)

    for i in xrange(len(sent[:ll])):
        axs[0].plot((i+0.5,i+0.5),(0,1),'w', color='#f1f1f2', linewidth=0.9)
        if sent[i] == "#": 
            axs[0].plot((i,i),(0,1),'w', color='grey', linewidth=10, alpha=0.2)

    axs[0].set_xlim([-0.5, len(sent[:ll])-0.5])
    axs[0].set_ylim([0,1])
    axs[0].set_ylabel('Cumulative RR')
    axs[0].get_xaxis().set_visible(False)
    #box = axs[0].get_position()
    #axs[0].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    #axs[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.9),fancybox=True, ncol=3)
    axs[0].legend(loc='lower right', prop={'size':8}, ncol=3)

    p1 = np.array(p1[:ll])
    p2 = np.array(p2[:ll])
    p3 = np.array(p3[:ll])
    cum1 = [np.sum(p1[:i])/np.sum(p1) for i in xrange(len(p1))]
    cum2 = [np.sum(p2[:i])/np.sum(p2) for i in xrange(len(p2))]
    cum3 = [np.sum(p3[:i])/np.sum(p3) for i in xrange(len(p3))]
    x = range(len(p1))
    sl10, _, _, _, _ = stats.linregress(x[:int(len(x)/2)],cum1[:int(len(x)/2)])
    sl11, _, _, _, _ = stats.linregress(x[int(len(x)/2):],cum1[int(len(x)/2):]) 
    sl1_all, _, _, _, _ = stats.linregress(x,cum1)
    sl20, _, _, _, _ = stats.linregress(x[:int(len(x)/2)],cum2[:int(len(x)/2)])
    sl21, _, _, _, _ = stats.linregress(x[int(len(x)/2):],cum2[int(len(x)/2):])
    sl2_all, _, _, _, _ = stats.linregress(x,cum2)
    sl30, _, _, _, _ = stats.linregress(x[:int(len(x)/2)],cum3[:int(len(x)/2)])
    sl31, _, _, _, _ = stats.linregress(x[int(len(x)/2):],cum3[int(len(x)/2):])
    sl3_all, _, _, _, _ = stats.linregress(x,cum3)
#    lb1 = '{}: {:0.1f} \n{}: {:0.1f}'.format(r'$1^{st}$', sl10/sl1_all*100, r'$2^{nd}$', sl11/sl1_all*100) 
#    lb2 = '{}: {:0.1f} \n{}: {:0.1f}'.format(r'$1^{st}$', sl20/sl2_all*100, r'$2^{nd}$', sl21/sl2_all*100) 
#    lb3 = '{}: {:0.1f} \n{}: {:0.1f}'.format(r'$1^{st}$', sl30/sl3_all*100, r'$2^{nd}$', sl31/sl3_all*100) 

    slopes = [sl10, sl20, sl30, sl11, sl21, sl31]
    sgn = []
    for sl in slopes:
        if sl < 100:
            sgn.append("-")
        else:
            sgn.append("+")

    lb1 = '{}: {} \n{}: {}'.format(r'$1^{st}$', sgn[0], r'$2^{nd}$', sgn[1]) 
    lb2 = '{}: {} \n{}: {}'.format(r'$1^{st}$', sgn[2], r'$2^{nd}$', sgn[3]) 
    lb3 = '{}: {} \n{}: {}'.format(r'$1^{st}$', sgn[4], r'$2^{nd}$', sgn[5]) 
    plt1 = axs[1].plot(ind, cum1, 'r-o', color='magenta', label=lb1, linewidth=lwd, alpha=op)
    plt2 = axs[1].plot(ind, cum2, 'r-o', color=color_list['green'], label=lb2, linewidth=lwd, alpha=op)
    plt3 = axs[1].plot(ind, cum3, 'r-o', color='dodgerblue', label=lb3, linewidth=lwd, alpha=op)
    for i in xrange(len(sent[:ll])):
        axs[1].plot((i+0.5,i+0.5),(0,1),'w', color='#f1f1f2', linewidth=0.9)
        if sent[i] == "#": 
            axs[1].plot((i,i),(0,1),'w', color='grey', linewidth=10, alpha=0.2)

    axs[1].set_xlim([-0.5, len(sent[:ll])-0.5])
    axs[1].set_ylim([0,1])
    axs[1].set_ylabel('Cumulative Perplexity')
    axs[1].get_xaxis().set_visible(False)
    #box = axs[1].get_position()
    #axs[1].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    #axs[1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.9),fancybox=True, ncol=3)
    axs[1].legend(loc='lower right', prop={'size':8}, ncol=3)
    collabel = tuple(list(sent))
    rowlabels = ['ngrmLM', 'preLM', 'OCLM']
    a = np.chararray((3,len(sent[:ll])))
    a[0] = list(s1[:ll])
    a[1] = list(s2[:ll])
    a[2] = list(s3[:ll])
    axs[2].axis('tight')
    axs[2].axis('off')
    table = axs[2].table(cellText=a, colLabels=collabel, rowColours=colors ,rowLabels=rowlabels, loc='center', cellLoc='center')
    table.auto_set_font_size(True)
    table.set_fontsize(font_size)
    i=0
    for k, cell in six.iteritems(table._cells):
        cell.set_edgecolor(edge_color)
        if k[0] == 0 or k[1] < header_columns:
            cell.set_text_props(weight='bold', color='w', wrap=True)
            cell.set_facecolor(header_color)
            if k[1] < header_columns:
              cell.set_facecolor(colors[i])
              i+=1
        else:
            cell.set_facecolor(row_colors[k[0]%len(row_colors) ])

    fig.subplots_adjust(hspace=0)
    plt.savefig(name+'.pdf')
    #plt.show()

