import sys
import json
import matplotlib.pyplot as plt
import numpy as np
from rrhist import hist
import math
from two_cumulative import cumulative2

fname = sys.argv[1]
fname1 = sys.argv[2]
fname2 = sys.argv[3]
outdir = sys.argv[4]
# read json file

with open(fname,"r") as datafile:
    data = json.load(datafile)
# show sentence length distribution
with open(fname1, "r") as datafile:
    data1 = json.load(datafile)
with open(fname2, "r") as datafile:
    data2 = json.load(datafile)

mrrs = []
mrrs1 = []
mrrs2 = []
rr = []
rr1 = []
rr2 = []
pp = []
pp1 = []
pp2 = []
for i, (sent, sent1, sent2) in enumerate(zip(data, data1, data2)):
    mrr = sent["mrr"]
    mrr1 = sent1["mrr"]
    mrr2 = sent2["mrr"]
    hyp = sent["lm_guess"]
    hyp1 = sent1["lm_guess"]
    hyp2 = sent2["lm_guess"]
    ranks = sent["inv_ranks"]
    ranks1 = sent1["inv_ranks"]
    ranks2 = sent2["inv_ranks"]
    ed = sent["edit"]
    ed1 = sent1["edit"]
    ed2 = sent2["edit"]
    orig = sent["tst"]
    orig1 = sent1["tst"]
    ppx = sent["ppx_single"]
    ppx1 = sent1["ppx_single"]
    ppx2 = sent2["ppx_single"]
    mrrs.append(mrr)
    mrrs1.append(mrr1)
    mrrs2.append(mrr2)
    rr.extend(ranks)
    rr1.extend(ranks1)
    rr2.extend(ranks2)
#    ppx = [math.pow(math.exp(1), nll) for nll in ppx]
#    ppx1 = [math.pow(math.exp(1), nll) for nll in ppx1]
    pp.extend(ppx)
    pp1.extend(ppx1)
    pp2.extend(ppx2)
    if 1:
      cumulative2(orig, hyp, hyp1, hyp2, ranks, ranks1, ranks2, ppx, ppx1, ppx2, ed, ed1, ed2, outdir+"/"+"cuml_"+str(i))
if 1:
  hist(pp, pp1, pp2, "Symbol Perplexity", "ppx", 100, outdir+"/") 
  hist(rr, rr1, rr, "Reciprocal Rank", "RR", 200, outdir+"/") 
