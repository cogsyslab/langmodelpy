from __future__ import division
import json
import re
import sys
from operator import itemgetter

def sent_tuples(fname):
  with open(fname, "r") as datafile:
    data2 = json.load(datafile)
  all_words = []
  for sent2 in data2:
    ranks = sent2["inv_ranks"]
    sentence = sent2["tst"]
    if len(sentence)>300:
      continue
    try:
      ratk = sent2["recall_at_k_sent"]
    except:
      ratk = sent2["recall_at_k"]/len(sentence)
    all_words.append((sentence, ranks, ratk))
  all_words = sorted(all_words, key=itemgetter(0))
  return all_words 

def word_tuples(fname):
  with open(fname, "r") as datafile:
    data2 = json.load(datafile)
  all_words = []
  for sent2 in data2:
    ranks = sent2["inv_ranks"]
    sentence = sent2["tst"]
    idxs = [i for i, x in enumerate(sentence) if x == "#"]
    idx_old = 0
    for idx in idxs:
      all_words.append((sentence[idx_old:idx+1], ranks[idx_old:idx+1]))
      idx_old = idx+1
    all_words.append((sentence[idx_old:], ranks[idx_old:]))
  all_words = sorted(all_words, key=itemgetter(0))
  return all_words 

def word_ppx(fname):
  with open(fname, "r") as datafile:
    data2 = json.load(datafile)
  all_words = []
  for sent2 in data2:
    ranks = sent2["ppx_single"]
    sentence = sent2["tst"]
    idxs = [i for i, x in enumerate(sentence) if x == "#"]
    idx_old = 0
    for idx in idxs:
      all_words.append((sentence[idx_old:idx+1], ranks[idx_old:idx+1]))
      idx_old = idx+1
    all_words.append((sentence[idx_old:], ranks[idx_old:]))
  all_words = sorted(all_words, key=itemgetter(0))
  return all_words 
