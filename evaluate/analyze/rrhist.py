import matplotlib.pyplot as plt
import matplotlib.ticker as ptick
from matplotlib import colors as mcolors

colors = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)

def hist(xx, xx1, xx2, title, xlab, num, outdir):
 
    f = plt.figure(num)
    #fig, ax = plt.subplots(2,1)
    ax0 = f.add_subplot(211)

    ax0.hist(xx, bins='auto', alpha=0.5, label='ngramLM', color=colors['green'])
    ax0.hist(xx1, bins='auto', alpha=0.5, label='preLM', color='magenta')
    ax0.hist(xx2, bins='auto', alpha=0.5, label='OCLM', color='green')
    ax0.legend(loc='upper left')
    ax0.set_title("LMs "+title)
    ax0.set_xlabel(xlab)
    ax0.set_ylabel("count")


    mf = ptick.ScalarFormatter(useMathText=True)
    mf.set_powerlimits((-2,2))
    ax0.yaxis.set_major_formatter(mf)

    ax1 = f.add_subplot(212) 
    ax1.hist(xx, bins='auto', cumulative=True, alpha=0.5, label='ngramLM', color=colors['green'])
    ax1.hist(xx1, bins='auto', cumulative=True, alpha=0.5, label='preLM', color='magenta')
    ax1.hist(xx2, bins='auto', cumulative=True, alpha=0.5, label='OCLM', color='green')
    ax1.legend(loc='upper left')
    ax1.set_title("Cumulative LMs "+title)
    ax1.set_xlabel(xlab)
    ax1.set_ylabel("count")
    title = title.replace(" ", "_")

    mf = ptick.ScalarFormatter(useMathText=True)
    mf.set_powerlimits((-2,2))
    ax1.yaxis.set_major_formatter(mf)
    plt.savefig(outdir+title+"_cum.pdf")
