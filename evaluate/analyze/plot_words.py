from __future__ import division
import sys
from preprocess import word_tuples
import pandas as pd
import matplotlib.pyplot as plt
import pdb
import math 
import numpy as np
import seaborn as sns
#import matplotlib

#matplotlib.rcParams.update({'font.size': 152})
sns.set(style="darkgrid")


fname1 = sys.argv[1]
fname2 = sys.argv[2]

w_pairs1 = word_tuples(fname1)
w_pairs2 = word_tuples(fname2)

ft = 20
if 0:
  fig = plt.figure(1, figsize=(10,10))
  mrr = []
  w_len = []
  mrr2 = []
  for (tgt, pred), (tgt1, pred1) in zip(w_pairs1, w_pairs2):
    ln = len(tgt)
    ln1 = len(tgt1)
    w_len.append(ln)
    mrr.append(sum(pred)/ln)
    mrr2.append(sum(pred1)/ln1)
  df = pd.DataFrame({'length': w_len, 'word MRR': mrr, ' ': mrr2 })
  #df = pd.DataFrame({'length': w_len, 'word MRR': mrr, 'word MRR2': mrr2 })

  ax1 = fig.add_subplot(3,1,1)
  bo = df.boxplot(column='word MRR', by='length', ax=ax1)
  #bo = df.boxplot(column='word MRR', by='length', ax=ax1)
  ax1.set_xlabel("")
  ax1.set_ylabel("OCLM", fontsize=ft)
  ax1.set_title("word MRR", fontsize=ft)
  ax1.tick_params(labelsize='large')
  fig.suptitle("")

  ax2 = fig.add_subplot(3,1,2)
  bo = df.boxplot(column=' ', by='length', ax=ax2)
  #bo = df.boxplot(column='word MRR2', by='length', ax=ax2)
  ax2.set_xlabel("")
  ax2.set_ylabel("PRE", fontsize=ft)
  ax2.tick_params(labelsize='large')
  fig.suptitle("")

  ax3 = fig.add_subplot(3,1,3)
  sns.countplot(x="length", data=df, ax=ax3, facecolor="grey")
  plt.yscale('log')
  ax3.tick_params(labelsize='large')
  ax3.set_xlabel("length", size=ft)
  ax3.set_ylabel("count", size=ft)
  plt.savefig("word.pdf", format='pdf', dpi=100)

else:
  #fig = plt.figure(2)
  fig = plt.figure(2, figsize=(10,10))
  rr = []
  rr2 = []
  pos = []
  for (tgt, pred), (tgt1, pred1) in zip(w_pairs1, w_pairs2):
    for i , (t, p, t1, p1) in  enumerate(zip(tgt, pred, tgt1, pred1)):
      pos.append(i+1)
      rr.append(p) 
      rr2.append(p1) 
  df = pd.DataFrame({'position': pos, 'position RR': rr, ' ': rr2})

  ax1 = fig.add_subplot(3,1,1)
  bo = df.boxplot(column='position RR', by='position', ax=ax1)
  ax1.set_xlabel("")
  ax1.set_ylabel("OCLM", fontsize=ft)
  ax1.tick_params(labelsize='large')
#  ax1.grid(linestile='-', linewidth=)
  ax1.set_title("RRs of letter positions within a word", fontsize=ft)
  plt.suptitle("")

  ax2 = fig.add_subplot(3,1,2)
  bo = df.boxplot(column=' ', by='position', ax=ax2)
  ax2.set_xlabel("")
  ax2.set_ylabel("PRE", fontsize=ft)
  ax2.tick_params(labelsize='large')
  plt.suptitle("")

  ax3 = fig.add_subplot(3,1,3)
  sns.countplot(x="position", data=df, ax=ax3, facecolor="grey")
  plt.yscale('log')
  ax3.tick_params(labelsize='large')
  ax3.set_xlabel("position", size=ft)
  ax3.set_ylabel("count", size=ft)
  plt.savefig("letter.pdf", format='pdf')
