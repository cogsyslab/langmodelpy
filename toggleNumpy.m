function outArray = toggleNumpy(inArray, varargin)
% matlab is a bit lame when it comes to converting to numpy, it only takes
% vectors:
%
% http://www.mathworks.com/help/matlab/matlab_external/passing-data-to-python.html
%
% this function toggles and array between a numpy and MATLAB state

p = inputParser;
p.addParameter('verboseFlag', true, @islogical);
p.parse(varargin{:});

if isnumeric(inArray)
    % MATLAB input given, build 2d python numpy array
    outArray = py.numpy.array(inArray(:)');
    outArray = outArray.reshape(size(inArray), pyargs('order','F'));
    return
end

% Python numpy array given, convert to MATLAB array

dim = py.len(inArray.shape);
assert(ismember(dim, 1:2), 'only 1 or 2 dimensional array supported (empty?)');
% we may also support 3d+ arrays ... just haven't tested yet

% http://www.mathworks.com/matlabcentral/answers/157347-convert-python-numpy-array-to-double
% d is for double, see link below on types
outArray = double(py.array.array('d', py.numpy.nditer(inArray)));
shape = double(py.array.array('d', py.list(inArray.shape)));

switch dim
    case 1
        % python has 1d arrays (all of MATLABs arrays are at least 2d),
        % for this reason there is ambiguity as to whether we should
        % make a row or column vector out of a 1d array ...
        if p.Results.verboseFlag
            warning('1d numpy array passed, building col vector (maybe input was row?)');
        end
        outArray = outArray(:);
    otherwise
        outArray = reshape(outArray, shape);
end

end